/***************************************************************************
                          cone3d.h  -  description
                             -------------------
    copyright            : (C) 2001 by Marius Andra aka Cone3D
    email                : marius@hot.ee
    ICQ                  : 43999510
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __CONE3D_H__
#define __CONE3D_H__

#ifdef WIN32
#include <windows.h>
#endif

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cone3dfont.h"
#include "cone3dmd2.h"
#include "cone3dpcx.h"

#define TRUE 1
#define FALSE 0
#define BOOL int

class Cone3D
{
public:
  Cone3D(){};
  ~Cone3D(){};
  GLvoid KillGLWindow(GLvoid);
  BOOL CreateGLWindow(char* title, int width, int height, int bits,
                      BOOL fullscreenflag);

private:
  int InitGL(GLvoid);
  void ReSizeGLScene(GLsizei width, GLsizei height);
};

#endif
