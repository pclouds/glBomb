#pragma once
#define MAX_OBJ 10
#define MAX_STEP 100
#define MAX_VERB 64  // IMPORTANT
#define MAX_FSTATE 32
#define MAX_ACTOR 4

#define LEFT 0
#define UP 1
#define DOWN 2
#define RIGHT 3
#define OPPOSITE(dir) (3 - (dir))

// Object
#define VERB_MOVE 1
#define VERB_PUSH 2
#define VERB_TOUCH 3 /* data = passby -> return = stop? */
#define VERB_BOMB_BOOM 4
#define VERB_STAY 5  // Ngừng di chuyển
#define VERB_END_STATE 6
#define VERB_BEGIN_STATE 7
#define VERB_REWIND_STATE 8
#define VERB_INPUT 9  // Sent by InputManager

// Container
#define VERB_OUT 11
#define VERB_IN 12          // Khi new Object thì tự động được gọi
#define VERB_POS 13         // Lấy tọa độ, data = struct* {int ,int}
#define VERB_PLAYGROUND 14  // Lấy Playground, data = Playground**
#define VERB_DESTROY 15     // Báo cho parent biết cần phải hủy nó.
#define VERB_END_GAME 16    // Kết thúc trò chơi, data = (int)is_win

// Bomber
#define VERB_BOMB_MAKE 20
#define VERB_GO 21

// Cell
#define VERB_DANGEROUS 30

// Gift
#define VERB_GIFT_BLOOD 40
#define VERB_GIFT_PUSH 41
#define VERB_GIFT_BOMB 42

// Live cycle
#define STATE_MASK 0xF000
#define STATE_VISIBLE 0x8000  // Hoạt động
#define STATE_PREDEAD 0x4000  // Sắp chết, không còn tác động.
#define STATE_DEAD 0x2000     // Chờ được xóa

// Capability
#define STATE_PUSH 0x0800  // Has ability to push anything
#define STATE_PASSBY 0x0400
#define STATE_MOVING 0x0200
#define STATE_DIRECT 0x0100
#define STATE_SECOND_RENDER 0x0080

// Actor's state
#define STATE_ACTOR_MOVING 1  // UP is still being pushed

// Bomb's state
#define STATE_BOMB_BOOM 1
#define STATE_PUSHING 2  // Be being pushed

// Playground's state
#define STATE_END_GAME 1
#define STATE_WIN_GAME 2

// Motion State
#define MSTATE_STAND 0
#define MSTATE_MOVE 1
#define MSTATE_BOMB 2
#define MSTATE_PAIN 3
#define MSTATE_PAIN2 4
#define MSTATE_PAIN3 5
#define MSTATE_DEAD 6
#define MSTATE_DEAD2 7
#define MSTATE_DEAD3 8
#define MSTATE_PUSH 9
#define MSTATE_GIFT 10

#define FSTATE_NOBREAK 1

// Local Variables:
// mode: c++
// End:
