#include "base.h"

#include <stdlib.h>
#include <string.h>

#include "lisptree.h"
#include "misc.h"

IMPLEMENT_ROOT(Base);

int Base::Serialize(lisp_object_t* cons, int store)
{
  return 1;
}

// Store: return new cons in chain. cons is (SYMBOL (SYMBOL DATA) ...)
// Load: return cons.

int LoadData(lisp_object_t* children, const char* name, int type, void* value)
{
  lisp_object_t* c;

  c = lisp_get_prop(children, name);
  if (c && lisp_type(c) == type)
  {
    switch (type)
    {
      case LISP_TYPE_INTEGER:
        *(int*)value = lisp_integer(c);
        break;
      case LISP_TYPE_REAL:
        *(float*)value = lisp_real(c);
        break;
      case LISP_TYPE_CONS:
        *(lisp_object_t**)value = c;
        break;
      case LISP_TYPE_STRING:
        *(char**)value = newstrdup(lisp_string(c));
        break;
    }
    return 1;
  }

  if (!c)
  {
    fprintf(stderr, "LoadData failed: %s not found.\n", name);
  }
  else
  {
    fprintf(stderr,
            "LoadData failed: type of %s is incorrect (%d instead of %d)",
            name,
            lisp_type(c),
            type);
  }
  return 0;
}

void StoreData(lisp_object_t* cons, const char* name, int type, void* value)
{
  lisp_object_t* c;

  switch (type)
  {
    case LISP_TYPE_INTEGER:
      c = lisp_make_integer(*(int*)value);
      break;
    case LISP_TYPE_REAL:
      c = lisp_make_real(*(float*)value);
      break;
    case LISP_TYPE_CONS:
      c = *(lisp_object_t**)value;
      break;
    default:
      abort();
  }
  lisp_add_child(cons, lisp_make_prop(name, c));
}

Base* Base::LoadObject(lisp_object_t* child, Base* parent)
{
  char type[100];
  lisp_object_t* cons;
  cons = lisp_get_object(child, type);
  if (strcmp(type, "object"))
  {
    return nullptr;
  }
  lisp_object_t* name = lisp_get_prop(cons, "object-class");
  if (name == nullptr || !lisp_symbol_p(name))
  {
    return nullptr;
  }
  RTTI* rtti = RTTI::Get(lisp_symbol(name));
  if (!rtti)
  {
    return nullptr;
  }
  Base* obj = rtti->CreateInstance();
  obj->Create(parent);
  if (!obj->Serialize(cons))
  {
    delete obj;
    return nullptr;
  }
  return obj;
}

lisp_object_t* Base::StoreObject()
{
  lisp_object_t* cons;
  Base* obj = this;

  cons = lisp_make_object("object");
  if (!obj->Serialize(cons, 1))
  {
    lisp_free(cons);
    return nullptr;
  }
  lisp_add_child(
      cons,
      lisp_make_prop("object-class", lisp_make_symbol(obj->MyClass()->Name())));
  return cons;
}
