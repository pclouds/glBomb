#include "gate.h"

#include <GL/glu.h>
#include <stdlib.h>

#include "actor.h"
#include "bot.h"
#include "explosion.h"
#include "playground.h"

// BRICK:
// Đây là phần tử thường gặp trong trò chơi.
// Ngăn cản (TOUCH) không cho đi xuyên qua (trừ khi có STATE_PASSBY).
// Chịu tác động (VERB_BOMB_BOOM) và biến mất.
// Chịu tác động của (VERB_PUSH). Khi đó sẽ tự động di chuyển và phát sinh
// OBSTACLE_PUSH khi đụng các vật thể khác.

IMPLEMENT(Gate, Object);

Base* Gate::Create(Base* p)
{
  Object::Create(p);
  blood = -1;
  new_creatures = 0;
  return this;
}

int Gate::DoFillInfo(ObjectResource* info)
{
  //  info->dplist = glGenLists(1);

  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND, 0, 1, 1);
  info->mstate[MSTATE_MOVE] = new FState(MSTATE_MOVE, 0, 1, 1);

  /*
  GLUquadricObj *obj;
  obj = gluNewQuadric();
  glNewList(info->dplist,GL_COMPILE);
  glColor3f (0,0,1);
  glTranslatef(0,-0.5,0);
  glRotatef(-90,1,0,0);
  gluCylinder(obj,.5,0,1,5,10);
  glEndList();
  gluDeleteQuadric(obj);
*/
  return 1;
}

void Gate::DoRender(int order)
{
  GLfloat x, y, z;
  GLfloat color[3] = {1, 0, 0};
  int i;

  x = (GLfloat)(rand() % 100) / 500 - 0.1;
  y = (GLfloat)(rand() % 100) / 500 - 0.1;
  z = (GLfloat)(rand() % 100) / 300 - 0.5;

  for (i = 0; i < 3; i++)
  {
    color[i] = (GLfloat)(rand() % 100) / 100;
  }

  glBegin(GL_TRIANGLE_FAN);
  glColor3fv(color);
  glVertex3f(x, 0.3, y);
  glColor3d(1, 1, 1);
  glVertex3f(-0.15, -0.5, -0.15);
  glVertex3f(-0.15, -0.5, 0.15);
  glVertex3f(0.15, -0.5, 0.15);
  glVertex3f(0.15, -0.5, -0.15);
  glVertex3f(-0.15, -0.5, -0.15);
  glEnd();
}

int Gate::DoAction(Object* obj, int verb, void* data)
{
  switch (verb)
  {
    case VERB_TOUCH:
      // nếu data != NULL nghĩa là có thể đi xuyên qua
      // Trả về non-zero nếu phải đứng lại (sờ thấy được)
      return 0;

    case VERB_PUSH:
      if (parent && CAST(Cell, parent)->N() > 1)
      {
        break;
      }
      if (obj->MyClass()->DerivedFrom(Actor::Class()))
      {
        Playground* pg = GetPlayground();
        pg->Action(this, VERB_END_GAME, (void*)1);
        obj->Action(this, VERB_GIFT_BLOOD, nullptr);  // mortality
      }
      break;

    case VERB_BOMB_BOOM:
      if (parent && CAST(Cell, parent)->N() > 2)
      {
        break;
      }
      new_creatures += rand() % 3;
      break;

    default:
      return Object::DoAction(obj, verb, data);
  }
  return 1;
}

void Gate::DoRun()
{
  if (new_creatures)
  {
    Cell* p = (Cell*)parent;
    int i;
    for (i = 0; i < MAX_OBJ; i++)
    {
      if (p->Exist(i) && (*p)[i]->MyClass() == Explosion::Class())
      {
        break;
      }
    }
    if (i == MAX_OBJ)
    {
      new_creatures--;
      (new Bot)->Create(parent);
    }
  }
}

// Local Variables:
// End:
