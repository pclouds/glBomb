#ifndef __RTTI_H__
#define __RTTI_H__

#define DECLARE(CLASS, PARENT) \
public:                        \
  static RTTI* Class();        \
  virtual RTTI* MyClass()

#define IMPLEMENT(CLASS, PARENT) \
  extern RTTI PARENT##_RTTI;     \
  extern RTTI CLASS##_RTTI;      \
  RTTI* CLASS::Class()           \
  {                              \
    return &CLASS##_RTTI;        \
  }                              \
  RTTI* CLASS::MyClass()         \
  {                              \
    return Class();              \
  }                              \
  Base* CLASS##_CreateInstance() \
  {                              \
    return new CLASS;            \
  }                              \
  RTTI CLASS##_RTTI(#CLASS, &PARENT##_RTTI, CLASS##_CreateInstance)

#define IMPLEMENT_ROOT(CLASS)    \
  extern RTTI CLASS##_RTTI;      \
  RTTI* CLASS::Class()           \
  {                              \
    return &CLASS##_RTTI;        \
  }                              \
  Base* CLASS##_CreateInstance() \
  {                              \
    return new CLASS;            \
  }                              \
  RTTI CLASS##_RTTI(#CLASS, NULL, CLASS##_CreateInstance)

#define CAST(CLASS, OBJECT) ((CLASS*)(OBJECT))

class Base;
class RTTI;

int RTTI_Init();
void RTTI_Cleanup();
RTTI* RTTI_Get(char* name);
int RTTI_From(RTTI* child, RTTI* parent);

class RTTI
{
public:
  RTTI()
  {
  }
  RTTI(const char* name, RTTI* parent, Base* (*c)());
  static RTTI* Get(const char* name);

  Base* (*CreateInstance)();

  int DerivedFrom(RTTI* parent)
  {
    return RTTI_From(this, parent);
  }
  RTTI* Parent()
  {
    return parent;
  }
  const char* Name()
  {
    return name;
  }

private:
  const char* name;
  RTTI* parent;
  RTTI* next;
};

#endif

// Local Variables:
// mode: c++
// End:
