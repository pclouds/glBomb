#include "renderman.h"

#include <algorithm>
#include <vector>

#define MAX_LAYER 3

static std::vector<Object*> layer[MAX_LAYER];
static int lookat_x, lookat_y;

int RenderManager_Init()
{
  RenderManager_Reset();
  return 1;
}

void RenderManager_Reset()
{
  lookat_x = lookat_y = 0;
  for (int i = 0; i < MAX_LAYER; i++)
  {
    layer[i].clear();
  }
}

void RenderManager_Cleanup()
{
}

int RM_Connect(int order, Object* obj)
{
  if (order >= MAX_LAYER)
  {
    return 0;
  }
  std::vector<Object*>::iterator result =
      find(layer[order].begin(), layer[order].end(), obj);
  if (result != layer[order].end())
  {
    return 0;
  }

  layer[order].push_back(obj);
  return 1;
}

void RM_Disconnect(int order, Object* obj)
{
  if (order >= MAX_LAYER)
  {
    return;
  }

  while (1)
  {
    std::vector<Object*>::iterator result =
        find(layer[order].begin(), layer[order].end(), obj);
    if (result == layer[order].end())
    {
      break;
    }

    layer[order].erase(result);
  }
}

void RenderManager_Render(int i)
{
  if (!layer[i].empty())
  {
    std::vector<Object*>::const_iterator iter;

    for (iter = layer[i].begin(); iter != layer[i].end(); ++iter)
    {
      (*iter)->Render(i);
    }
  }
}

void RM_LookAt(int x, int y)
{
  lookat_x = x;
  lookat_y = y;
}

int RM_LookAtX()
{
  return lookat_x;
}
int RM_LookAtY()
{
  return lookat_y;
}
