#pragma once
#include "gift.h"
#include "object.h"

class BombGift : public Gift
{
public:
  Base* Create(Base* p) override;

protected:
  int DoFillInfo(ObjectResource*) override;
  int DoAction(Object* obj, int verb, void* data) override;

private:
  DECLARE(BombGift, Gift);
};

// Local Variables:
// mode: c++
// End:
