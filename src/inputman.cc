#include "inputman.h"

#include <algorithm>
#include <vector>

#define MAX_LAYOUT 32
#define MAX_HOOK 8

struct InputLayout
{
  std::vector<Object*> obj;
};

static InputLayout layout[MAX_LAYOUT];
static int current_layout;

int InputManager_Init()
{
  InputManager_Reset();
  return 1;
}

void InputManager_Cleanup()
{
}

void InputManager_Process(SDL_Event* ev)
{
  std::vector<Object*>& o = layout[current_layout].obj;
  std::vector<Object*>::const_iterator i;
  for (i = o.begin(); i != o.end(); ++i)
  {
    if (!(*i)->Action(nullptr, VERB_INPUT, ev))
    {
      return;
    }
  }
}

void InputManager_Reset()
{
  current_layout = 0;
  IM_ResetLayout();
}

void IM_ResetLayout()
{
  layout[current_layout].obj.clear();
}

int IM_PushLayout()
{
  if (current_layout + 1 >= MAX_LAYOUT)
  {
    return 0;
  }
  current_layout++;
  layout[current_layout] = layout[current_layout - 1];
  return 1;
}

int IM_PopLayout()
{
  if (current_layout == 0)
  {
    return 0;
  }
  current_layout--;
  return 1;
}

int IM_Connect(Object* obj)
{
  if (obj == nullptr)
  {
    return 0;
  }

  std::vector<Object*>& o = layout[current_layout].obj;

  if (find(o.begin(), o.end(), obj) == o.end())
  {
    o.push_back(obj);
  }
  return 1;
}

int IM_Disconnect(Object* obj)
{
  std::vector<Object*>& o = layout[current_layout].obj;
  std::vector<Object*>::iterator iter;

  iter = find(o.begin(), o.end(), obj);
  if (iter != o.end())
  {
    o.erase(iter);
    return 1;
  }
  else
  {
    return 0;
  }
}
