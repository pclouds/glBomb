#include "screen.h"

Cone3DFont font;

int screen_begin(int w, int h)
{
  static int loaded = 0;
  if (!loaded)
  {
    loaded = 1;
    font.BuildFont("data/font.pcx");
  }

  glPushAttrib(GL_ENABLE_BIT);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_CULL_FACE);
  glDisable(GL_LIGHTING);
  glMatrixMode(GL_PROJECTION);
  glPushMatrix();
  glLoadIdentity();
  glOrtho(0, w, h, 0, -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();
  glLoadIdentity();
  return 1;
}

void screen_end()
{
  glPopAttrib();
  glMatrixMode(GL_PROJECTION);
  glPopMatrix();
  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
}
/*
int install_texture(char *filename,int &w,int &h,int clamp)
{
  SDL_Surface *s = SDL_LoadBMP(filename);
  if(!s) {
    fprintf(stderr,"Couldn't load texture %s\n", (int)filename);
    return 0;
  }

  if(s->format->BitsPerPixel!=24) {
    fprintf(stderr,"texture must be 24bpp: %s\n", (int)texname);
    SDL_FreeSurface(s);
    return 0;
  }

  // loopi(s->w*s->h*3) { uchar *p = (uchar *)s->pixels+i; *p = 255-*p; };
  glBindTexture(GL_TEXTURE_2D,tnum);
  glPixelStorei(GL_UNPACK_ALIGNMENT,1);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, clamp ? GL_CLAMP_TO_EDGE :
GL_REPEAT); glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, clamp ?
GL_CLAMP_TO_EDGE : GL_REPEAT); glTexParameteri(GL_TEXTURE_2D,
GL_TEXTURE_MAG_FILTER, GL_LINEAR); glTexParameteri(GL_TEXTURE_2D,
GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); //NEAREST);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  if(gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB, s->w, s->h, GL_RGB,
GL_UNSIGNED_BYTE, s->pixels)) { fprintf(stderr,"could not build mipmaps\n");
    SDL_FreeSurface(s);
    return 0;
  }
  w = s->w;
  h = s->h;
  SDL_FreeSurface(s);
  return 1;
}

*/
