#ifndef __BOMBER_H__
#define __BOMBER_H__
#include "object.h"

class Bomber : public Object
{
public:
  Base* Create(Base* p) override;
  ~Bomber() override;

  int values[10];

protected:
  int Serialize(lisp_object_t*, int store = 0) override;
  int DoFillInfo(ObjectResource* info) override;

  int DoAction(Object* obj, int verb, void* data) override;
  void MakeBomb();
  void DoRun() override;
  int bomb_power;

private:
  DECLARE(Bomber, Object);
};

#endif

// Local Variables:
// mode: c++
// End:
