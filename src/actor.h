#pragma once
#include <SDL2/SDL.h>

#include "bomber.h"

class Actor : public Bomber
{
public:
  Base* Create(Base* p) override;
  ~Actor() override;

protected:
  int DoFillInfo(ObjectResource*) override;
  int DoAction(Object* sender, int verb, void* data) override;
  int InputAction(SDL_Event* event);
  void DoRun() override;
  void DoRender(int order) override;

  void ConnectRenderer() override;
  void DisconnectRenderer() override;

private:
  DECLARE(Actor, Bomber);
};

// Local Variables:
// mode: c++
// End:
