#include "bomb.h"

#include <GL/glu.h>
#include <stdio.h>
#include <stdlib.h>

#include "explosion.h"
#include "playground.h"
#include "renderman.h"

// BOMB:
// Tự động nổ sau 10 nhịp

IMPLEMENT(Bomb, Object);

Base* Bomb::Create(Base* p)
{
  Object::Create(p);
  ChangeState(MSTATE_STAND);
  thresold = 50;
  //  explosion_thresold = 2;
  blood = -1;
  return this;
}

Bomb::~Bomb()
{
}

int Bomb::DoFillInfo(ObjectResource* info)
{
  GLUquadricObj* obj;
  GLfloat specular[] = {1, 1, 0, 1};

  info->dplist = glGenLists(2);
  obj = gluNewQuadric();

  glNewList(info->dplist, GL_COMPILE);
  glColor3f(1, 0, 0);
  glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
  glTranslatef(0, -0.5, 0);
  gluSphere(obj, 0.3, 10, 10);
  glEndList();

  glNewList(info->dplist + 1, GL_COMPILE);
  glColor3f(1, 1, 0);
  glTranslatef(0, -0.5, 0);
  gluSphere(obj, 0.3, 10, 10);
  glEndList();

  gluDeleteQuadric(obj);

  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND, 0, 25, 2);
  info->mstate[MSTATE_BOMB] = new FState(MSTATE_BOMB, 0, 0, 1);
  info->mstate[MSTATE_STAND]->next = info->mstate[MSTATE_BOMB];
  return 1;
}

void Bomb::DoRender(int order)
{
  if (motion.current && motion.current->id == MSTATE_STAND)
  {
    glCallList(info->dplist + (motion.step & 1));
  }
}

void Bomb::DoRun()
{
  /*
  if (state & STATE_BOMB_BOOM) {
    BoomBoom();
    return;
  }
  */

  Playground* pg;
  Position p;
  GetPosition(p);
  pg = GetPlayground();
  pg->c(p)->Action(this, VERB_DANGEROUS, (void*)1);
  for (int i = 0; i < 4; i++)
  {
    int distance = explosion_thresold;
    Position target(p);
    while (distance)
    {
      target += next_position[i];
      if (!pg->Exist(target))
      {
        distance = 0;
      }
      else
      {
        std::shared_ptr<Cell> c = pg->c(target);
        c->Action(this, VERB_DANGEROUS, (void*)1);
        distance--;
        if (c->Touch(this))
        {
          distance = 0;
        }
      }
    }
  }
}

int Bomb::DoAction(Object* o, int verb, void* data)
{
  FMachine* fmachine;
  switch (verb)
  {
    case VERB_PUSH:
      Boom();
      break;

    case VERB_GO:
    case VERB_BOMB_BOOM:
      if ((state & STATE_BOMB_BOOM) == 0)
      {
        Boom();
      }
      break;

    case VERB_BEGIN_STATE:
      fmachine = (FMachine*)data;
      switch (fmachine->current->id)
      {
        case MSTATE_BOMB:
          if ((state & STATE_BOMB_BOOM) == 0)
          {
            Boom();
          }
          break;
        default:
          Object::DoAction(o, verb, data);
      }
      break;

    case VERB_TOUCH:
      return 0;

    default:
      return Object::DoAction(o, verb, data);
  }
  return 1;
}

void Bomb::Boom()
{
  Position p;
  if (!GetPosition(p))
  {
    return;
  }

  state |= STATE_BOMB_BOOM;

  for (int i = 0; i < 4; i++)
  {
    distance[i] = explosion_thresold;
    boom[i] = p;
  }

  (new Explosion)->Create(parent);
  parent->Action(this, VERB_DANGEROUS, nullptr);

  // this is from BoomBoom
  Playground* pg = GetPlayground();
  for (int i = 0; i < 4; i++)
  {
    int distance = explosion_thresold;
    Position target(p);
    while (distance)
    {
      target += next_position[i];
      if (!pg->Exist(target))
      {
        distance = 0;
      }
      else
      {
        std::shared_ptr<Cell> c = pg->c(target);
        c->Action(this, VERB_DANGEROUS, nullptr);
        (new Explosion((explosion_thresold - distance + 1) * 2))
            ->Create(c.get());
        distance--;
        if (c->Touch(this))
        {
          distance = 0;
        }
      }
    }
  }

  SetLiveState(STATE_PREDEAD);
}

void Bomb::BoomBoom()
{
  int done = 1;
  Playground* pg;

  pg = GetPlayground();
  if (!pg)
  {
    return;
  }

  for (int i = 0; i < 4; i++)
  {
    if (distance[i])
    {
      done = 0;
      boom[i] += next_position[i];
      if (!pg->Exist(boom[i]))
      {
        distance[i] = 0;
      }
      else
      {
        std::shared_ptr<Cell> c = pg->c(boom[i]);
        c->Action(this, VERB_DANGEROUS, nullptr);
        (new Explosion)->Create(c.get());
        distance[i]--;
        if (c->Touch(this))
        {
          distance[i] = 0;
        }
      }
    }
  }

  if (done)
  {
    SetLiveState(STATE_PREDEAD);
  }
}

// Explosion:
// Tự động nổ sau 10 nhịp

IMPLEMENT(Explosion, Object);

Base* Explosion::Create(Base* p)
{
  Object::Create(p);

  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND, 0, wait, 1);
  info->mstate[MSTATE_STAND]->next = info->mstate[MSTATE_BOMB];

  ChangeState(MSTATE_STAND);
  thresold = 15;
  // SetPredead();
  /*
  if (p != NULL && wait == 0)
    ((Object*)p)->Action(this,VERB_BOMB_BOOM,NULL);
  */
  return this;
}

Explosion::~Explosion()
{
}

void Explosion::ConnectRenderer()
{
  RM_Connect(RENDER_ENTITY, this);
}

void Explosion::DisconnectRenderer()
{
  RM_Disconnect(RENDER_ENTITY, this);
}

int Explosion::DoFillInfo(ObjectResource* info)
{
  info->mstate[MSTATE_BOMB] = new FState(MSTATE_BOMB, 1, 3, 1);

  // info->mstate[MSTATE_STAND]->sfx = sfx_load("data/explosive.sfx");
  return 1;
}

void Explosion::DoRender(int order)
{
  if (!motion.current || motion.current->id != MSTATE_BOMB)
  {
    return;
  }

  GLfloat emission[] = {1, 1, 0, 1};
  glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT);
  glDisable(GL_CULL_FACE);
  // glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBegin(GL_TRIANGLES);
  for (int i = 0; i < 15; i++)
  {
    GLfloat x, y, z;
    x = (GLfloat)rand() / RAND_MAX;
    y = (GLfloat)rand() / RAND_MAX;
    z = (GLfloat)rand() / RAND_MAX;
    glColor4f(x, y, z, .5);
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emission);
    glVertex3f(x, y, z);
    x = (GLfloat)rand() / RAND_MAX;
    y = (GLfloat)rand() / RAND_MAX;
    z = (GLfloat)rand() / RAND_MAX;
    glVertex3f(x, y, z);
    x = (GLfloat)rand() / RAND_MAX;
    y = (GLfloat)rand() / RAND_MAX;
    z = (GLfloat)rand() / RAND_MAX;
    glVertex3f(x, y, z);
  }
  glEnd();
  glPopAttrib();
}

int Explosion::DoAction(Object* o, int verb, void* data)
{
  switch (verb)
  {
    case VERB_TOUCH:
      return 0;

    case VERB_GO:
      o->Action(this, VERB_BOMB_BOOM, (void*)OPPOSITE((intptr_t)data));
      break;

    case VERB_BOMB_BOOM:
      return 1;

    case VERB_BEGIN_STATE:
      if (((FMachine*)data)->current->id == MSTATE_BOMB && parent)
      {
        parent->Action(this, VERB_BOMB_BOOM, nullptr);
      }
      break;

    case VERB_REWIND_STATE:
      SetLiveState(STATE_PREDEAD);
      break;

    default:
      return Object::DoAction(o, verb, data);
  }
  return 0;
}

// Local Variables:
// End:
