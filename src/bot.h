#pragma once
#include "bomber.h"

class Bot : public Bomber
{
public:
  Base* Create(Base* p) override;
  ~Bot() override;

protected:
  int DoFillInfo(ObjectResource* info) override;
  int DoAction(Object* obj, int verb, int* data);
  void DoRun() override;

private:
  DECLARE(Bot, Bomber);
};

// Local Variables:
// mode: c++
// End:
