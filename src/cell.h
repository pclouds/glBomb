#ifndef __CELL_H__
#define __CELL_H__
#include "object.h"

/*
   Cell có thể chứa 1 vật cản và 1 đối tượng.
   Ngoài ra cell cũng có thể chứa các dead object (các object chỉ dùng
   để vẽ, không tồn tại trong trò chơi.

   Parent của Cell là Playground.
*/
class Cell : public Object
{
public:
  Base* Create(Base* p) override;

  Object* operator[](int i)
  {
    return obj[i];
  }
  int Exist(int i)
  {
    return (i < MAX_OBJ) && (obj[i] != nullptr);
  }
  int N()
  {
    int cnt = 0;
    for (int i = 0; i < MAX_OBJ; i++)
    {
      if (Exist(i))
      {
        cnt++;
      }
    }
    return cnt;
  }

protected:
  Object* obj[MAX_OBJ]; /* Các obj không tồn tại = NULL */
  int destroyed_obj[MAX_OBJ];

  int Serialize(lisp_object_t*, int store = 0) override;
  int DoFillInfo(ObjectResource*) override;

  void DoRender(int order) override;
  int DoAction(Object* obj, int verb, void* data) override;
  void DoRun() override;

private:
  DECLARE(Cell, Object);
};

#endif

// Local Variables:
// mode: c++
// End:
