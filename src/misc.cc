#include "misc.h"

#include <string.h>

char* newstrdup(const char* str)
{
  if (!str)
  {
    return nullptr;
  }

  int len = strlen(str);
  char* buf = new char[len + 1];
  strcpy(buf, str);
  return buf;
}
