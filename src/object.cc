#include "object.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "lisptree.h"
#include "playground.h"
#include "renderman.h"

// OBJECT:
// Đây là lớp cơ sở của mọi vật thể trong trò chơi. Chịu trách nhiệm các phản
// ứng cơ bản như render, move, touch, in, out ...
// Khi gọi construtor thì tự động "đăng ký" (IN) với cell.
// Khi gọi destructor thì tự động "hủy đăng ký" (OUT) với cell.

#define SMOOTH_MOVE

static int next_direction[4][4] = {
    // LEFT UP OWN RIGHT
    {DOWN, LEFT, RIGHT, UP},  // LEFT
    {LEFT, UP, DOWN, RIGHT},  // UP
    {RIGHT, DOWN, UP, LEFT},  // DOWN
    {UP, RIGHT, LEFT, DOWN}   // RIGHT
};

Position next_position[4] = {
    Position(-1, 0),  //
    Position(0, -1),  //
    Position(0, 1),   //
    Position(+1, 0)   //
};

/*
int Object_DoFillInfo(ObjectResource *info) { return 1; }
Object* Object_CreateInstance() { return NULL; }

RTTI Object_RTTI("ObjectClass",NULL,Object_CreateInstance,Object_DoFillInfo);

RTTI* Object::Class() { return &Object_RTTI; }
*/
IMPLEMENT(Object, Base);

int Object::DoFillInfo(ObjectResource*)
{
  return 1;
}

int Object::FillInfo(ObjectResource* info)
{
  FILE* fp;
  char filename[100];

  sprintf(filename, "data/object/%s.cfg", MyClass()->Name());
  fp = fopen(filename, "rb");
  if (fp)
  {
    printf("Loading %s..\n", MyClass()->Name());

    lisp_stream_t st;
    lisp_stream_init_file(&st, fp);
    lisp_object_t* obj = lisp_get_object(lisp_read(&st), nullptr);
    if (obj)
    {
      info->Serialize(obj, 0);
      lisp_free(obj);
    }
    fclose(fp);
  }

  DoFillInfo(info);

  if (info->md2_filename)
  {
    char md2[100], tex[100];
    info->md2_model = new Cone3DMD2;
    sprintf(md2, "data/models/%s/tris.md2", info->md2_filename);
    sprintf(tex, "data/models/%s/tris.pcx", info->md2_filename);
    info->md2_model->LoadModel(md2, tex);
    if (access(md2, R_OK))
    {
      sprintf(md2, "data/models/%s/weapon.md2", info->md2_filename);
      sprintf(tex, "data/models/%s/weapon.pcx", info->md2_filename);
      info->md2_model->LoadWeapon(md2, tex);
    }
  }

  for (int i = 0; i < MAX_FSTATE; i++)
  {
    if (info->sfx_filename[i] && info->mstate[i])
    {
      info->mstate[i]->sfx = Mix_LoadWAV(info->sfx_filename[i]);
      if (!info->mstate[i]->sfx)
      {
        fprintf(stderr, "SFX %s error\n", info->sfx_filename[i]);
      }
      else
      {
        printf("Attach %s to %s (%d)\n",
               info->sfx_filename[i],
               MyClass()->Name(),
               i);
      }
    }
  }

  return 1;
}
// Registration mechanism
/*
#define MAX_REGISTERED_CLASS 32
static struct _tagRegister {
  char* cls;
  ObjectResource* data;
} reg[MAX_REGISTERED_CLASS];
*/
int common_init()
{
  /*
  for (int i = 0;i < MAX_REGISTERED_CLASS;i ++)
    reg[i].cls = NULL;
  */
  return 1;
}

void common_cleanup()
{
}

/*
ObjectResource* Object::CheckRegister()
{
  return CheckRegister(MyClass());
}

ObjectResource* Object::CheckRegister(char *cls)
{
  int null_room = MAX_REGISTERED_CLASS;

  for (int i = 0;i < MAX_REGISTERED_CLASS;i ++) {
    if (reg[i].cls == NULL)
      null_room = i;
    else
      if (!strcmp(cls,reg[i].cls))
        return reg[i].data;
  }

  if (null_room != MAX_REGISTERED_CLASS) {
    reg[null_room].cls = cls;
    reg[null_room].data = NewInfo();
    DoFillInfo(reg[null_room].data);
    return reg[null_room].data;
  }
  return NULL;
}
char* Object::Class() { return "ObjectClass"; }
*/

Base* Object::Create(Base* p)
{
  motion.Create(this);
  animation.Create(this);

  info = CAST(ObjectResource, Res_Get(MyClass()->Name()));
  if (!info)
  {
    info = new ObjectResource;
    FillInfo(info);
    Res_Register(MyClass()->Name(), info);
  }
  //  printf("%s created info %p func %p\n",MyClass(),info,info->Create);

  state = 0;
  x = y = 0;       // Can not move
  direction = UP;  // by default
  dx = dy = 20;
  parent = nullptr;
  blood = 1;
  move_step = 0;
  move_thresold = 3;
  move_distance = -1;

  for (int i = 0; i < MAX_VERB; i++)
  {
    verb_mask[i] = ~(STATE_DEAD);  // Enable all but STATE_DEAD
  }
  verb_mask[VERB_TOUCH] &= ~STATE_PREDEAD;

  SetLiveState(STATE_VISIBLE);

  if (p)
  {
    ((Object*)p)->Action(this, VERB_IN, nullptr);
  }

  return this;
}

Object::~Object()
{
  if (parent)
  {
    parent->Action(this, VERB_OUT, nullptr);
  }
}

void Object::Render(int order)
{
  static GLfloat alpha[4] = {180, 90, -90, 0};

  glPushMatrix();
  glPushAttrib(GL_ALL_ATTRIB_BITS);

  if (order != RENDER_SCREEN)
  {
    if (parent)
    {
      Position p;
      GetPosition(p);
      SetPos(p.x, p.y);
    }

#ifdef SMOOTH_MOVE
    if (x || y)
    {
      glTranslatef((GLfloat)x / MAX_STEP, 0, (GLfloat)y / MAX_STEP);
      if ((state & STATE_DIRECT) && direction != RIGHT)
      {
        glRotatef(alpha[direction], 0, 1, 0);
      }
    }
    else
    {
      if ((state & STATE_DIRECT) && direction != RIGHT)
      {
        glRotatef(alpha[direction], 0, 1, 0);
      }
    }
#else
    if ((state & STATE_DIRECT) && direction != RIGHT)
      glRotatef(alpha[direction], 0, 1, 0);
#endif
  }

  DoRender(order);
  glPopAttrib();
  glPopMatrix();
}

// Use info->md2_model by default
void Object::DoRender(int order)
{
  glPushAttrib(GL_ENABLE_BIT);
  glEnable(GL_TEXTURE_2D);
  glTranslatef(0, 0.3, 0);
  glColor3d(1, 1, 1);
  glScalef(info->md2_scale, info->md2_scale, info->md2_scale);
  info->md2_model->SetFrame(motion.step);
  info->md2_model->RenderCurrentFrame(
      0, 0, 0, motion.current && motion.current->id != MSTATE_DEAD);
  //  glBindTexture(GL_TEXTURE_BINDING_2D,0);
  glPopAttrib();
}

void Object::Move(Object* o, int dir)
{
  Position p;
  std::shared_ptr<Cell> c;
  Playground* pg;

  if (dir != UP)
  {
    // printf("Direction %d -> %d\n",direction,next_direction[direction][dir]);
    direction = next_direction[direction][dir];
    return;
  }

  // direction = dir;
  //   printf("Direction %d\n",direction);

  if (!parent->Action(this, VERB_PLAYGROUND, &pg))
  {
    return;
  }

  if (state & STATE_PUSHING)
  {
    move_distance--;
    if (move_distance < 0)
    {
      Action(this, VERB_STAY, nullptr);
      move_distance = -1;
    }
  }

  switch (direction)
  {
    case LEFT:
      GetPosition(p);
      //    parent->Action(this,VERB_POS,&p);
      p += next_position[direction];
      if (pg->Exist(p))
      {
        c = pg->c(p);
        if (!c->Action(this, VERB_TOUCH, &p))
        {
#ifdef SMOOTH_MOVE
          x -= dx;
          if (x < 0 && -x > MAX_STEP / 2)
          {
#endif
            parent->Action(this, VERB_OUT, nullptr);
            c->Action(this, VERB_IN, nullptr);
            c->Action(this, VERB_GO, nullptr);
#ifdef SMOOTH_MOVE
            x = MAX_STEP + x;
            y = 0;  // Quite bad!
          }
#endif
        }
        else
        {
          Action(this, VERB_STAY, nullptr);
#ifdef SMOOTH_MOVE
          if (x > 0)
          {
            x -= dx;
            if (x < 0)
            {
              x = 0;
            }
          }
#endif
        }
      }
      else
      {
        Action(this, VERB_STAY, nullptr);
#ifdef SMOOTH_MOVE
        if (x > 0)
        {
          x -= dx;
          if (x < 0)
          {
            x = 0;
          }
        }
#endif
      }
      break;

    case RIGHT:
      GetPosition(p);
      //    parent->Action(this,VERB_POS,&p);
      p += next_position[direction];
      if (pg->Exist(p))
      {
        c = pg->c(p);
        if (!c->Action(this, VERB_TOUCH, &p))
        {
#ifdef SMOOTH_MOVE
          x += dx;
          if (x > MAX_STEP / 2)
          {
#endif
            parent->Action(this, VERB_OUT, nullptr);
            c->Action(this, VERB_IN, nullptr);
            c->Action(this, VERB_GO, nullptr);
#ifdef SMOOTH_MOVE
            x = x - MAX_STEP;
            y = 0;
          }
#endif
        }
        else
        {
          Action(this, VERB_STAY, nullptr);
#ifdef SMOOTH_MOVE
          if (x < 0)
          {
            x += dx;
            if (x > 0)
            {
              x = 0;
            }
          }
#endif
        }
      }
      else
      {
        Action(this, VERB_STAY, nullptr);
#ifdef SMOOTH_MOVE
        if (x < 0)
        {
          x += dx;
          if (x > 0)
          {
            x = 0;
          }
        }
#endif
      }
      break;

    case UP:
      GetPosition(p);
      //    parent->Action(this,VERB_POS,&p);
      p += next_position[direction];
      if (pg->Exist(p))
      {
        c = pg->c(p);
        if (!c->Action(this, VERB_TOUCH, &p))
        {
#ifdef SMOOTH_MOVE
          y -= dy;
          if (y < 0 && -y > MAX_STEP / 2)
          {
#endif
            parent->Action(this, VERB_OUT, nullptr);
            c->Action(this, VERB_IN, nullptr);
            c->Action(this, VERB_GO, nullptr);
#ifdef SMOOTH_MOVE
            y = MAX_STEP + y;
            x = 0;  // Quite bad!
          }
#endif
        }
        else
        {
          Action(this, VERB_STAY, nullptr);
#ifdef SMOOTH_MOVE
          if (y > 0)
          {
            y -= dy;
            if (y < 0)
            {
              y = 0;
            }
          }
#endif
        }
      }
      else
      {
        Action(this, VERB_STAY, nullptr);
#ifdef SMOOTH_MOVE
        if (y > 0)
        {
          y -= dy;
          if (y < 0)
          {
            y = 0;
          }
        }
#endif
      }
      break;

    case DOWN:
      //    printf("Down %d %d\n",x,y);
      GetPosition(p);
      //    parent->Action(this,VERB_POS,&p);
      p += next_position[direction];
      if (pg->Exist(p))
      {
        c = pg->c(p);
        if (!c->Action(this, VERB_TOUCH, &p))
        {
#ifdef SMOOTH_MOVE
          y += dy;
          if (y > MAX_STEP / 2)
          {
#endif
            parent->Action(this, VERB_OUT, nullptr);
            c->Action(this, VERB_IN, nullptr);
            c->Action(this, VERB_GO, nullptr);
#ifdef SMOOTH_MOVE
            y = y - MAX_STEP;
            x = 0;
          }
#endif
        }
        else
        {
          Action(this, VERB_STAY, nullptr);
#ifdef SMOOTH_MOVE
          if (y < 0)
          {
            y += dy;
            if (y > 0)
            {
              y = 0;
            }
          }
#endif
        }
      }
      else
      {
        Action(this, VERB_STAY, nullptr);
#ifdef SMOOTH_MOVE
        if (y < 0)
        {
          y += dy;
          if (y > 0)
          {
            y = 0;
          }
        }
#endif
      }
      break;
  }
}

int Object::Action(Object* o, int verb, void* data)
{
  return CanAction(o, verb, data) ? DoAction(o, verb, data) : 0;
}

int Object::DoAction(Object* o, int verb, void* data)
{
  FMachine* fmachine;

  switch (verb)
  {
    case VERB_MOVE:
      if ((intptr_t)data == UP)
      {
        move_step = 0;
        ChangeState(MSTATE_MOVE);
      }
      else
      {
        Move(o, (intptr_t)data);
      }
      break;

    case VERB_STAY:
      ChangeState(MSTATE_STAND);
      if (state & STATE_PUSHING)
      {
        state &= ~STATE_PUSHING;

        Playground* pg = GetPlayground();
        Position p;
        GetPosition(p);
        p += next_position[direction];
        if (pg->Exist(p))
        {
          pg->c(p)->Action(this, VERB_PUSH, (void*)direction);
        }
      }
      break;

    case VERB_PUSH:
      if (o && (o->State() & STATE_PUSH) && info->mstate[MSTATE_MOVE])
      {
        state |= STATE_PUSHING;
        move_distance = 6;
        direction = (intptr_t)data;
        move_step = 0;
        ChangeState(MSTATE_MOVE);
      }
      break;

    case VERB_TOUCH:
      return 1;

    case VERB_IN:
      o->parent = this;
      break;

    case VERB_OUT:
      o->parent = nullptr;
      break;

    case VERB_BOMB_BOOM:
      if (blood > 0)
      {
        blood--;
        ChangeState(MSTATE_PAIN);
        if (blood == 0)
        {
          SetLiveState(STATE_PREDEAD);
        }
      }
      break;

    case VERB_REWIND_STATE:
      fmachine = (FMachine*)data;
      switch (fmachine->current->id)
      {
          //    case MSTATE_MOVE:Move(o,UP);break;
        case MSTATE_DEAD:
          SetLiveState(STATE_PREDEAD);
          break;
      }
      break;

    case VERB_GIFT_BLOOD:
      if ((intptr_t)data)
      {
        blood += (intptr_t)data;
      }
      else
      {
        blood = -1;
      }
      ChangeState(MSTATE_GIFT);
      break;

    case VERB_GIFT_PUSH:
      state |= STATE_PUSH;
      ChangeState(MSTATE_GIFT);
      break;

    default:
      return 0;
  }
  return 1;
}

Playground* Object::GetPlayground()
{
  Playground* pg = nullptr;
  if (parent)
  {
    parent->Action(this, VERB_PLAYGROUND, &pg);
  }
  return pg;
}

/*
void Object::Send(Object *o,int v,void *data)
{
  SDL_mutexP(verb_mutex);
  if (nr_verb < MAX_VERB) {
    verb[nr_verb].sender = o;
    verb[nr_verb].verb = v;
    verb[nr_verb].data = data;
    nr_verb ++;
  }
  SDL_mutexV(verb_mutex);
}

int Object::GetVerb(Verb &v)
{
  SDL_mutexP(verb_mutex);
  if (nr_verb) {
    v = verb[nr_verb-1];
    nr_verb --;
  }
  SDL_mutexV(verb_mutex);
  return 0;
}
*/

int Object::GetPosition(Position& p)
{
  /*
  p = pos;
  return 1;
  */
  if (parent == nullptr)
  {
    return 0;
  }
  return parent->Action(this, VERB_POS, &p);
}

void Object::ConnectRenderer()
{
  RM_Connect(RENDER_LANDSCAPE, this);
}

void Object::DisconnectRenderer()
{
  RM_Disconnect(RENDER_LANDSCAPE, this);
}

int Object::SetLiveState(int _state)
{
  state = (state & ~STATE_MASK) | (_state & STATE_MASK);

  if (state & STATE_PREDEAD)
  {
    if (!ChangeState(MSTATE_DEAD))
    {
      SetLiveState(STATE_DEAD);
    }
  }

  if (state & STATE_DEAD)
  {
    DisconnectRenderer();
    parent->Action(this, VERB_DESTROY, nullptr);
    //    printf("Destroy %s\n",MyClass()->Name());
  }

  if (state & STATE_VISIBLE)
  {
    ConnectRenderer();
  }

  return 1;
}

int Object::CanAction(Object* obj, int verb, void* data)
{
  return (verb_mask[verb] & state) & STATE_MASK;
}

int Object::CanRender()
{
  return state & (STATE_VISIBLE | STATE_PREDEAD);
}

int Object::CanRun()
{
  return (state & STATE_DEAD) == 0;
}

void Object::Run()
{
  motion.Step();
  animation.Step();

  if (motion.current != nullptr && motion.current->id == MSTATE_MOVE)
  {
    move_step++;
    if (move_step > move_thresold)
    {
      move_step = 0;
      Move(nullptr, UP);
    }
  }

  if (CanRun())
  {
    DoRun();
  }
}

void Object::DoRun()
{
}

IMPLEMENT(ObjectResource, Base);

ObjectResource::ObjectResource()
{
  md2_scale = 2;
  for (int i = 0; i < MAX_FSTATE; i++)
  {
    mstate[i] = nullptr;
    astate[i] = nullptr;
    sfx_filename[i] = nullptr;
  }
  md2_model = nullptr;
  md2_filename = nullptr;
}

ObjectResource::~ObjectResource()
{
  for (int i = 0; i < MAX_FSTATE; i++)
  {
    if (mstate[i])
    {
      delete mstate[i];
    }
    if (astate[i])
    {
      delete astate[i];
    }
    if (sfx_filename[i])
    {
      delete[] sfx_filename[i];
    }
  }
  if (md2_model)
  {
    delete md2_model;
  }
  if (md2_filename)
  {
    delete[] md2_filename;
  }
}

int ObjectResource::Serialize(lisp_object_t* cons, int store)
{
  lisp_object_t* objs = lisp_nil();

  BEGIN_PROPS(props)
  PROP_REAL("model-scale", md2_scale)
  PROP_STR("model", md2_filename)
  PROP_CONS("sound", objs)
  END_PROPS();

  Base::Serialize(cons, store);

  if (store)
  {
    for (int i = 0; i < MAX_FSTATE; i++)
    {
      if (sfx_filename[i])
      {
        lisp_object_t* o = lisp_nil();
        o = lisp_make_cons(lisp_make_string(sfx_filename[i]), o);
        o = lisp_make_cons(lisp_make_integer(i), o);
        objs = lisp_make_cons(o, objs);
      }
    }
    SERIALIZE(props);
  }
  else
  {
    SERIALIZE(props);
    while (!lisp_nil_p(objs))
    {
      lisp_object_t* o = lisp_car(objs);
      int i = lisp_integer(lisp_car(o));
      sfx_filename[i] = newstrdup(lisp_string(lisp_car(lisp_cdr(o))));
      objs = lisp_cdr(objs);
    }
  }
  return 1;
}

int Object::ChangeState(int st)
{
  switch (st)
  {
    case MSTATE_PAIN:
      st += rand() % 3;
      break;
    case MSTATE_DEAD:
      st += rand() % 3;
      break;
  }

  if (info->mstate[st])
  {
    return motion.Change(info->mstate[st]);
  }
  else
  {
    return 0;
  }
}

int Object::Serialize(lisp_object_t* cons, int store)
{
  BEGIN_PROPS(props)
  PROP_INT("dx", dx)
  PROP_INT("dy", dy)
  PROP_INT("x", x)
  PROP_INT("y", y)
  PROP_INT("direction", direction)
  PROP_INT("state", state)
  PROP_INT("move_step", move_step)
  PROP_INT("move_thresold", move_thresold)
  PROP_INT("blood", blood)
  END_PROPS();

  Base::Serialize(cons, store);
  SERIALIZE(props);
  return 1;
}

void Object::SetPos(int _x, int _y)
{
  glTranslated(_x, 0, _y);
}
