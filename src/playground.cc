#include "playground.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "actor.h"
#include "bomb.h"
#include "bombgift.h"
#include "bot.h"
#include "brick.h"
#include "gate.h"
#include "gift.h"
#include "inputman.h"
#include "lisptree.h"
#include "object.h"
#include "pushgift.h"
#include "stone.h"

// PLAYGROUND:
// Là nền. Chứa các cell. Có thể là các cell rời rạc. Playground được dùng để
// cung cấp thông tin cho các vật thể khác (tọa độ, ...)

#define EFFECT_BACKGROUND 1
#define EFFECT_LIGHT 2

Playground::Playground(int screenWidth, int screenHeight)
    : m_screenWidth(screenWidth), m_screenHeight(screenHeight)
{
  Object::Create(nullptr);
  state |= STATE_SECOND_RENDER;
  m_texture.LoadPCX("data/bg.pcx");
  IM_Connect(this);
}

int Playground::Exist(int x, int y)
{
  return (x < m_width && y < m_height && x >= 0 && y >= 0) &&
         c(x, y) != nullptr;
}

void Playground::DoRender(int order)
{
  return;
}

int Playground::DoAction(Object* obj, int verb, void* data)
{
  int i, j;

  switch (verb)
  {
    case VERB_INPUT:
    {
      SDL_Event* ev = (SDL_Event*)data;
      if (ev->type == SDL_KEYDOWN && (ev->key.keysym.mod & KMOD_CTRL))
      {
        switch (ev->key.keysym.sym)
        {
          case SDLK_b:
            effect = (effect & ~EFFECT_BACKGROUND) |
                     ((effect ^ EFFECT_BACKGROUND) & EFFECT_BACKGROUND);
            break;
          case SDLK_l:
            effect = (effect & ~EFFECT_LIGHT) |
                     ((effect ^ EFFECT_LIGHT) & EFFECT_LIGHT);
            break;
          default:
            break;
        }
      }
      break;
    }

    case VERB_POS:
      for (i = 0; i < m_width; i++)
      {
        for (j = 0; j < m_height; j++)
        {
          if (obj == c(i, j).get())
          {
            ((Position*)data)->x = i;
            ((Position*)data)->y = j;
            return 1;
          }
        }
      }
      return 0;

    case VERB_END_GAME:
      state |= STATE_END_GAME;
      if (data)
      {
        state |= STATE_WIN_GAME;
      }
      break;

      /*
    case VERB_IN:
      break;

    case VERB_OUT:
      break;
      */

    case VERB_DESTROY:
      for (auto& actor : m_actors)
      {
        if (actor == obj)
        {
          actor = nullptr;
          break;
        }
      }
      break;

    default:
      return Object::DoAction(obj, verb, data);
  }
  return 1;
}

// Chịu trách nhiệm gọi Cell:Run. Gọi Object::CanRun để xem có nên gọi Run của
// mấy thằng cell hay không.

void Playground::DoRun()
{
  for (auto& cell : m_table)
  {
    if (cell)
    {
      cell->Run();
    }
  }
}

int Playground::InitGame()
{
  // OpenGL setup
  glShadeModel(GL_SMOOTH);
  glCullFace(GL_BACK);
  glFrontFace(GL_CCW);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_COLOR_MATERIAL);
  glEnable(GL_FRAMEBUFFER_SRGB);

  glClearColor(0, .7, .4, 0);

  glViewport(0, 0, m_screenWidth, m_screenHeight);

  glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  return 1;
}

void Playground::CleanupGame()
{
  for (auto& actor : m_actors)
  {
    actor = nullptr;
  }
}

int Playground::NewGame(int _w, int _h)
{
  CleanupGame();

  m_width = _w;
  m_height = _h;
  m_table.clear();
  m_table.resize(m_width * m_height);

  return 1;
}

int Playground::CreateRandom()
{
#define ALLOW(i, j) ((i) > 1 && (j) > 1)
  int i, j;

  // Create Cell
  for (auto& cell : m_table)
  {
    cell = std::make_shared<Cell>();
    cell->Create(this);
  }

  // Fill cells with Brick first
  for (i = 0; i < m_width; i++)
  {
    for (j = 0; j < m_height; j++)
    {
      if (ALLOW(i, j) && Exist(i, j) && c(i, j)->Touch(this) == 0 &&
          (rand() % 4) == 0)
      {
        (new Brick)->Create(c(i, j).get());
      }
    }
  }

  // Place the gate
  int make_gate = 0;
  while (!make_gate)
  {
    for (i = 0; i < m_width; i++)
    {
      for (j = 0; j < m_height; j++)
      {
        if (Exist(i, j) && c(i, j)->Touch(this) && (rand() % 13) == 0)
        {
          (new Gate)->Create(c(i, j).get());
          make_gate = 1;
          break;
        }
      }
    }
  }

  // Fill cells with others
  for (i = 0; i < m_width; i++)
  {
    for (j = 0; j < m_height; j++)
    {
      if (ALLOW(i, j) &&  // Reserved (0,0) for Bomber
          Exist(i, j))
      {
        if (c(i, j)->Touch(this) == 0)
        {
          struct Cls
          {
            int rate;
            RTTI* rtti;
          };
          std::vector<Cls> cls = {
              {11, Stone::Class()},
              {21, Gift::Class()},
              {31, PushGift::Class()},
              {21, BombGift::Class()},
              {131, Bomb::Class()},
          };

          for (const auto& cc : cls)
          {
            if ((rand() % cc.rate) == 0)
            {
              reinterpret_cast<Object*>(cc.rtti->CreateInstance())
                  ->Create(c(i, j).get());
              break;
            }
          }
        }
      }
    }
  }

  (new Actor)->Create(c(0, 0).get());
  //  int b_id = SetActor(b1);

  for (i = 0; i < 10; i++)
  {
    int x = rand() % 10;
    int y = rand() % 10;
    if (Exist(x, y) && !c(x, y)->Touch(this))
    {
      (new Bot)->Create(c(x, y).get());
    }
  }

  return 1;
}

int Playground::SetActor(Object* o)
{
  for (size_t i = 0; i < m_actors.size(); i++)
  {
    if (m_actors[i] == nullptr)
    {
      m_actors[i] = o;
      return i;
    }
  }
  return -1;
}

int Playground::IsEndGame()
{
  return state & STATE_END_GAME;
}
