/***************************************************************************
                          cone3dfont.h  -  description
                             -------------------
    copyright            : (C) 2001 by Marius Andra aka Cone3D
    email                : marius@hot.ee
    ICQ                  : 43999510
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __CONE3D_FONT_H__
#define __CONE3D_FONT_H__
#include "cone3dpcx.h"

class Cone3DFont
{
public:
  Cone3DFont()
  {
    sizex = 1.0f;
    sizey = 1.0f;
  };
  ~Cone3DFont()
  {
    KillFont();
  };
  int BuildFont(const char* file);
  int PrintText(int x, int y, char type, const char* fmt, ...);
  int DrawText(char type, const char* fmt, ...);
  int KillFont();
  int SetSize(float x, float y);

  Cone3DPCX font;

private:
  int base;
  float sizex, sizey;
};

#endif
