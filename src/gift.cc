#include "gift.h"

#include <GL/glu.h>
#include <stdio.h>

#include "bomber.h"
#include "bombgift.h"
#include "pushgift.h"

// GIFT:
// Mặc định Gift sẽ tăng máu

IMPLEMENT(Gift, Object);

Base* Gift::Create(Base* p)
{
  Object::Create(p);
  blood = 1;
  ChangeState(MSTATE_STAND);
  return this;
}

Gift::~Gift()
{
  //  glDeleteLists(dplist,1);
}

int Gift::DoFillInfo(ObjectResource* info)
{
  GLfloat color[] = {1, 0, 0, 1};
  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND, 0, 2, 2);

  info->dplist = glGenLists(3);
  //  GLfloat emission[] = {.6,.7,.6,1};

  GLUquadricObj* obj;
  obj = gluNewQuadric();
  for (int i = 0; i < 3; i++)
  {
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glNewList(info->dplist + i, GL_COMPILE);
    if (i == 2)
    {
      glMaterialfv(GL_FRONT, GL_EMISSION, color);
    }
    else
    {
      glColor4fv(color);
    }
    // glColor3f(1,0,0);
    glTranslatef(0, -0.5, 0);
    glRotatef(120 * i, 0, 1, 0);
    glRotatef(-90, 1, 0, 0);
    gluCylinder(obj, .5, 0, 1, 4, 5);
    glEndList();
    glPopAttrib();
  }
  gluDeleteQuadric(obj);
  return 1;
}

void Gift::DoRender(int order)
{
  glCallList(info->dplist + motion.step);
}

int Gift::DoAction(Object* obj, int verb, void* data)
{
  switch (verb)
  {
    case VERB_TOUCH:
      return 1;

    case VERB_PUSH:
      if (obj && obj->MyClass()->DerivedFrom(Bomber::Class()))
      {
        obj->Action(this, VERB_GIFT_BLOOD, (void*)1);
        SetLiveState(STATE_PREDEAD);
      }
      break;

    default:
      return Object::DoAction(obj, verb, data);
  }
  return 0;
}

// PushGift:
// Cho phép bomber đẩy vật đi chỗ khác.

IMPLEMENT(PushGift, Gift);

int PushGift::DoFillInfo(ObjectResource* info)
{
  GLfloat color[] = {1, 0, 1, 1};

  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND, 0, 2, 2);

  info->dplist = glGenLists(3);

  GLUquadricObj* obj;
  obj = gluNewQuadric();
  for (int i = 0; i < 3; i++)
  {
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glNewList(info->dplist + i, GL_COMPILE);
    if (i == 2)
    {
      glMaterialfv(GL_FRONT, GL_EMISSION, color);
    }
    else
    {
      glColor4fv(color);
    }
    glTranslatef(0, -0.5, 0);
    glRotatef(120 * i, 0, 1, 0);
    glRotatef(-90, 1, 0, 0);
    gluCylinder(obj, .5, 0, 1, 3, 5);
    glEndList();
    glPopAttrib();
  }
  gluDeleteQuadric(obj);
  return 1;
}

Base* PushGift::Create(Base* p)
{
  Object::Create(p);
  blood = 1;
  ChangeState(MSTATE_STAND);
  return this;
}

int PushGift::DoAction(Object* o, int verb, void* data)
{
  if (verb != VERB_PUSH)
  {
    return Gift::DoAction(o, verb, data);
  }

  if (o && o->MyClass()->DerivedFrom(Bomber::Class()))
  {
    o->Action(this, VERB_GIFT_PUSH, nullptr);
    SetLiveState(STATE_PREDEAD);
  }
  return 1;
}

// BombGift:
// Tăng độ dài Bomb

IMPLEMENT(BombGift, Gift);

int BombGift::DoFillInfo(ObjectResource* info)
{
  GLfloat color[] = {.5, .5, .5, 1};

  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND, 0, 2, 2);

  info->dplist = glGenLists(3);

  GLUquadricObj* obj;
  obj = gluNewQuadric();
  for (int i = 0; i < 3; i++)
  {
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glNewList(info->dplist + i, GL_COMPILE);
    if (i == 2)
    {
      glMaterialfv(GL_FRONT, GL_EMISSION, color);
    }
    else
    {
      glColor4fv(color);
    }
    glTranslatef(0, -0.5, 0);
    glRotatef(120 * i, 0, 1, 0);
    glRotatef(-90, 1, 0, 0);
    gluCylinder(obj, .5, 0, 1, 3, 5);
    glEndList();
    glPopAttrib();
  }
  gluDeleteQuadric(obj);
  return 1;
}

Base* BombGift::Create(Base* p)
{
  Object::Create(p);
  blood = 1;
  ChangeState(MSTATE_STAND);
  return this;
}

int BombGift::DoAction(Object* o, int verb, void* data)
{
  if (verb != VERB_PUSH)
  {
    return Gift::DoAction(o, verb, data);
  }

  if (o && o->MyClass()->DerivedFrom(Bomber::Class()))
  {
    o->Action(this, VERB_GIFT_BOMB, nullptr);
    SetLiveState(STATE_PREDEAD);
  }
  return 1;
}

// Local Variables:
// End:
