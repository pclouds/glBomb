#pragma once
#include "lispreader.h"
#ifdef __cplusplus
extern "C"
{
#endif
  lisp_object_t* lisp_make_object(const char* type);
  void lisp_add_child(lisp_object_t* cons, lisp_object_t* child);
  lisp_object_t* lisp_make_prop(const char* name, lisp_object_t* value);
  lisp_object_t* lisp_get_object(lisp_object_t* cons, char* type);
  lisp_object_t* lisp_get_prop(lisp_object_t* cons, const char* name);
#ifdef __cplusplus
}
#endif
