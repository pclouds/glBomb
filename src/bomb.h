#pragma once
#include "object.h"

class Bomb : public Object
{
public:
  Bomb(int _range = 1)
  {
    explosion_thresold = _range;
  }
  Base* Create(Base* p) override;
  ~Bomb() override;

protected:
  int thresold, distance[4], explosion_thresold;
  Position boom[4];

  void DoRender(int order) override;
  int DoAction(Object* obj, int verb, void* data) override;
  void DoRun() override;

  int DoFillInfo(ObjectResource*) override;

  void Boom();
  void BoomBoom();

private:
  DECLARE(Bomb, Object);
};

// Local Variables:
// mode: c++
// End:
