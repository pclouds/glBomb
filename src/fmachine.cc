#include <signal.h>
#include <stdio.h>
#include <unistd.h>

#include "object.h"

IMPLEMENT(FState, Base);
IMPLEMENT(FMachine, Base);

FState::FState()
{
  sfx = nullptr;
  start = step = end = 0;
  next = this;  // loop
}

int FState::Serialize(lisp_object_t* cons, int store)
{
  BEGIN_PROPS(props)
  PROP_INT("id", id)
  PROP_INT("start", start)
  PROP_INT("end", end)
  PROP_INT("step", step)
  PROP_INT("delay", delay)
  PROP_INT("state", state)
  // PROP_OBJ("sfx",sfx)
  END_PROPS();

  Base::Serialize(cons, store);
  SERIALIZE(props);
  return 1;
}

Base* FMachine::Create(Base* _host)
{
  host = CAST(Object, _host);
  current = nullptr;
  step = 0;
  delay = 0;
  return this;
}

int FMachine::Change(FState* state)
{
  if (current && (current->state & FSTATE_NOBREAK))
  {
    return 0;
  }
  /*
  if (current && state)
    printf("Change state from %d to %d\n",current->id,state->id);
  */
  /*
  if (host->MyClass() == RTTI::Get("Explosion"))
    kill(getpid(),SIGINT);
  */

  current = state;
  step = current->start;
  delay = 0;
  BeginOfState();
  return 1;
}

void FMachine::Step()
{
  if (current == nullptr)
  {
    return;
  }
  delay++;
  if (delay > current->delay)
  {
    step++;
    delay = 0;
    if (step > current->end)
    {
      if (current->next != current)
      {
        EndOfState();
        Change(current->next);
      }
      else
      {
        RewindState();
        step = current->start;
        delay = 0;
      }
    }
  }
}

void FMachine::EndOfState()
{
  if (host != nullptr)
  {
    host->Action(nullptr, VERB_END_STATE, this);
  }
}

void FMachine::BeginOfState()
{
  if (current && current->sfx)
  {
    Mix_PlayChannel(-1, current->sfx, 0);
    printf("Play %s %d\n", host->MyClass()->Name(), current->id);
  }

  if (host != nullptr)
  {
    host->Action(nullptr, VERB_BEGIN_STATE, this);
  }
}

void FMachine::RewindState()
{
  /*
  if (current && current->sfx)
    sfx_add(current->sfx);
  */
  if (host != nullptr)
  {
    host->Action(nullptr, VERB_REWIND_STATE, this);
  }
}
