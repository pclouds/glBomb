#pragma once
#include "object.h"

class Stone : public Object
{
public:
  Base* Create(Base* p) override;
  ~Stone() override;

protected:
  void DoRender(int order) override;
  int DoAction(Object* obj, int verb, void* data) override;
  int DoFillInfo(ObjectResource* info) override;

private:
  DECLARE(Stone, Object);
};

// Local Variables:
// mode: c++
// End:
