#include "rtti.h"

#include <string.h>

#include "base.h"

static RTTI* root = nullptr;

int RTTI_Init()
{
  return 1;
}

void RTTI_Cleanup()
{
}

RTTI* RTTI::Get(const char* name)
{
  for (RTTI* rtti = root; rtti; rtti = rtti->next)
  {
    if (!strcmp(name, rtti->Name()))
    {
      return rtti;
    }
  }
  return nullptr;
}

int RTTI_From(RTTI* child, RTTI* parent)
{
  while (child && child != parent)
  {
    child = child->Parent();
  }
  return child != nullptr;
}

RTTI::RTTI(const char* _name, RTTI* _parent, Base* (*c)())
{
  name = _name;
  parent = _parent;
  CreateInstance = c;
  next = root;
  root = this;
  //  printf("Add %s\n",_name);
}
