#include <GL/glu.h>
#include <SDL2/SDL.h>
#include <math.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "cone3dmd2.h"
#include "inputman.h"
#include "playground.h"
#include "renderman.h"
#include "screen.h"

#define MAX_CHANNEL 2
#define SCR_WIDTH 800
#define SCR_HEIGHT 600

#define MAX_PRESSED 20

static SDL_Window* screen;
/*
static struct {
  SDL_KeyboardEvent ev;
  Uint32 ticks;
} pressed_map[MAX_PRESSED];
*/
#define PI 3.1416

Playground* pg;
GLfloat distance = 5;  // Số ô nhìn thấy
GLfloat alpha = 10, beta = 30, org_x, org_y;
int run = 1;
int bomber;
Object* actor;

int msg(const char* format, ...)
{
  int ret;
  va_list ap;

  va_start(ap, format);
  ret = vfprintf(stderr, format, ap);
  va_end(ap);
  return ret;
}

int init()
{
  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
  {
    msg("Couldn't initialize SDL: %s\n", SDL_GetError());
    return 0;
  }

  atexit(SDL_Quit);

  screen = SDL_CreateWindow("glBomb",
                            SDL_WINDOWPOS_UNDEFINED,
                            SDL_WINDOWPOS_UNDEFINED,
                            SCR_WIDTH,
                            SCR_HEIGHT,
                            /* SDL_WINDOW_FULLSCREEN | */ SDL_WINDOW_OPENGL);

  if (screen == nullptr)
  {
    msg("SDL_CreateWindow failed: %s\n", SDL_GetError());
    return 0;
  }

  SDL_GL_CreateContext(screen);

  if (!common_init())
  {
    return 0;
  }

  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  time_t t;
  time(&t);
  srand((int)t);

  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY /*spec.freq*/,
                    MIX_DEFAULT_FORMAT /*spec.format*/,
                    MAX_CHANNEL /*spec.channels*/,
                    4096 /*spec.size*/))
  {
    Mix_SetMusicCMD(getenv("MUSIC_CMD"));
  }

  ResManager_Init();

  if (!RTTI_Init())
  {
    return 0;
  }

  pg = new Playground(SCR_WIDTH, SCR_HEIGHT);
  return pg->InitGame();
}

void cleanup()
{
  pg->CleanupGame();
  RTTI_Cleanup();
  Mix_CloseAudio();
  common_cleanup();
}

void RenderScene()
{
  GLfloat alpha_rad = alpha * PI / 180, beta_rad = beta * PI / 180;
  GLfloat h, d;
  GLfloat ambient[] = {.3, .3, .3, 1};
  GLfloat diffuse[] = {.9, .9, .9, 1};
  GLfloat position[] = {1, 1, 1, 1};
  GLfloat specular[] = {0.3, 0.3, 0.3, 1.0};

  h = (GLfloat)10 /*distance*/ * sin(beta_rad);
  d = (GLfloat)10 /*distance*/ * cos(beta_rad);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  position[0] = 1;
  position[1] = 1;
  position[2] = 1;
  position[3] = 0;
  glLightfv(GL_LIGHT0, GL_POSITION, position);
  glLightfv(GL_LIGHT0, GL_AMBIENT, ambient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
  //  glLighti(GL_LIGHT0,GL_SPOT_CUTOFF,360);
  //  GLfloat spotdir[] = {1,1,1};
  //  glLightfv(GL_LIGHT0,GL_SPOT_DIRECTION,spotdir);

  glMatrixMode(GL_TEXTURE);
  glLoadIdentity();

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-distance,
          distance,
          -distance * SCR_HEIGHT / SCR_WIDTH,
          distance * SCR_HEIGHT / SCR_WIDTH,
          1,
          60);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  GLfloat eye_x, eye_y;
  Position p(RM_LookAtX(), RM_LookAtY());
  if (pg->IsEndGame())
  {
    p.x = pg->width() / 2;
    p.y = pg->height() / 2;
    alpha += 3;
  }
  eye_x = p.x + (GLfloat)d * cos(alpha_rad);
  eye_y = p.y + (GLfloat)d * sin(alpha_rad);
  gluLookAt(eye_x, h, eye_y, p.x, 0, p.y, 0, 1, 0);

  glBegin(GL_LINES);
  glColor3f(1, 0, 0);
  glVertex3i(0, 0, 0);
  glVertex3i(1, 0, 0);
  glColor3f(0, 1, 0);
  glVertex3i(0, 0, 0);
  glVertex3i(0, 1, 0);
  glColor3f(0, 0, 1);
  glVertex3i(0, 0, 0);
  glVertex3i(0, 0, 1);
  glEnd();

  /*
  glPushMatrix();
  pg->Render(RENDER_LANDSCAPE);
  glPopMatrix();
  */
  RenderManager_Render(RENDER_LANDSCAPE);
  RenderManager_Render(RENDER_ENTITY);

  /*
  glPushMatrix();
  glDisable(GL_CULL_FACE);
  glRotatef(180,0,0,1);
  glRotatef(90,0,1,0);
  font.DrawText(0,"Hello");
  glEnable(GL_CULL_FACE);
  glPopMatrix();
  */

  screen_begin(SCR_WIDTH, SCR_HEIGHT);
  glPushMatrix();
  RenderManager_Render(RENDER_SCREEN);
  if (pg->IsEndGame())
  {
    static int delay = 10;
    static GLfloat r = 1, g = 0, b = 0;
    char* str = (char*)(pg->IsWin() ? "You Won!" : "Game Over");
    int len = strlen(str);
    int sx = 5, sy = 5;
    font.SetSize(sx, sy);
    if (delay)
    {
      delay--;
    }
    else
    {
      r = (GLfloat)rand() / RAND_MAX;
      g = (GLfloat)rand() / RAND_MAX;
      b = (GLfloat)rand() / RAND_MAX;
      delay = 10;
    }
    glColor3f(r, g, b);
    glTranslated((SCR_WIDTH - len * sx * 16) / 2 + 16 * sx,
                 (SCR_HEIGHT - sy * 16) / 2,
                 0);
    font.DrawText(2, str);
  }
  glPopMatrix();
  font.SetSize(1, 1);
#define AUTHOR_STR "Nguyen Thai Ngoc Duy - Nguyen Ngoc Tung - Vu Giang Nam - "
  char* author = newstrdup(AUTHOR_STR);
  static int slide = 0;
  glColor3f((GLfloat)rand() / RAND_MAX,
            (GLfloat)rand() / RAND_MAX,
            (GLfloat)rand() / RAND_MAX);
  glTranslated(0, SCR_HEIGHT - 30, 0);
  font.DrawText(1, author + slide);
  author[slide] = 0;
  font.DrawText(1, author);
  slide++;
  if (slide == strlen(AUTHOR_STR))
  {
    slide = 0;
  }
  delete author;
  screen_end();
}

void EventHandler()
{
  SDL_Event ev;
  int hasEvent, genEvent;
  int i;
  static int running = 0;

  while (1)
  {
    genEvent = hasEvent = 0;

    if (SDL_PollEvent(&ev))
    {
      hasEvent = 1;
    }
    if (!hasEvent)
    {
      break;
    }

    InputManager_Process(&ev);

    switch (ev.type)
    {
      case SDL_KEYDOWN:
        // Autogenerate keys
        /*
        if (!genEvent)
          for (i = 0;i < MAX_PRESSED;i ++)
            if (pressed_map[i].ev.keysym.sym == SDLK_UNKNOWN) {
              pressed_map[i].ev = ev.key;
              pressed_map[i].ticks = new_ticks;
              break;
            }
        */
        if (ev.key.keysym.mod & KMOD_CTRL)
        {
          switch (ev.key.keysym.sym)
          {
            case SDLK_DOWN:
              beta -= 10;
              break;
            case SDLK_UP:
              beta += 10;
              break;
            case SDLK_LEFT:
              alpha -= 10;
              break;
            case SDLK_RIGHT:
              alpha += 10;
              break;
            default:
              break;
          }
        }
        else
        {
          switch (ev.key.keysym.sym)
          {
              /*
            case SDLK_UP: running = 1;
              if (pg->GetActor(bomber))
              pg->GetActor(bomber)->Action(pg,VERB_MOVE,(void*)UP);break;
            case SDLK_DOWN:
              if (pg->GetActor(bomber))
              pg->GetActor(bomber)->Action(pg,VERB_MOVE,(void*)DOWN);break;
            case SDLK_LEFT:
              if (pg->GetActor(bomber))
              pg->GetActor(bomber)->Action(pg,VERB_MOVE,(void*)LEFT);break;
            case SDLK_RIGHT:
              if (pg->GetActor(bomber))
              pg->GetActor(bomber)->Action(pg,VERB_MOVE,(void*)RIGHT);break;
            case SDLK_SPACE:
              if (pg->GetActor(bomber))
              pg->GetActor(bomber)->Action(pg,VERB_BOMB_MAKE,NULL);break;
            case SDLK_RETURN:
              if (pg->GetActor(bomber))
              pg->GetActor(bomber)->Action(pg,VERB_PUSH,(void*)UP);break;
              */
            case SDLK_PAGEUP:
              distance -= .5;
              break;
            case SDLK_PAGEDOWN:
              distance += .5;
              break;
            case SDLK_ESCAPE:
              run = 0;
              break;
            case SDLK_HOME:
            {
              static int count = 0;
              char str[128];
              SDL_Surface* sf;

              sprintf(str, "screenshot%d.bmp", count++);
              sf = SDL_CreateRGBSurface(SDL_SWSURFACE,
                                        SCR_WIDTH,
                                        SCR_HEIGHT,
                                        24,
                                        0x0000FF,
                                        0x00FF00,
                                        0xFF0000,
                                        0);
              SDL_LockSurface(sf);
              glReadPixels(0,
                           0,
                           SCR_WIDTH,
                           SCR_HEIGHT,
                           GL_RGB,
                           GL_UNSIGNED_BYTE,
                           sf->pixels);
              // SDL_BlitSurface(screen,NULL,sf,NULL);
              SDL_SaveBMP(sf, str);
              SDL_FreeSurface(sf);
              SDL_UnlockSurface(sf);
            }
            break;
            default:
              break;
          }
        }
        break;
        /*
      case SDL_KEYUP:
        if ((ev.key.keysym.mod & KMOD_CTRL) == 0)
          switch (ev.key.keysym.sym) {
          case SDLK_UP: running = 0;
          case SDLK_DOWN:
          case SDLK_LEFT:
          case SDLK_RIGHT:
            if (pg->GetActor(bomber))
            pg->GetActor(bomber)->Action(pg,VERB_STAY,NULL);break;
          }
        */
        /*
        for (i = 0;i < MAX_PRESSED;i ++)
          if (pressed_map[i].ev.keysym.sym == ev.key.keysym.sym &&
              pressed_map[i].ev.keysym.mod == ev.key.keysym.mod) {
            pressed_map[i].ev.keysym.sym = SDLK_UNKNOWN;
            break;
          }
        */
        break;
      default:
        break;
    }
  }
}

int main(int argc, char** argv)
{
  // int i,j;

  if (!init())
  {
    return -1;
  }

  if (argc < 2)
  {
    pg->NewGame(10, 10);
    pg->CreateRandom();
  }

  bomber = pg->SetActor(actor);

  org_x = pg->width() * 1.0 / 2;
  org_y = pg->height() * 1.0 / 2;

  /*

  (new Cell)->Create(NULL);
  (new Brick)->Create(NULL);
  (new Stone)->Create(NULL);
  //  (new Playground)->Create(NULL);
  (new Bomb)->Create(NULL);

  pg->LoadGame("sample");
  //(new Brick)->Create(pg->c(0,0));
  */
  Mix_Music* mus = Mix_LoadMUS("data/lvl3.mid");
  if (mus)
  {
    Mix_PlayMusic(mus, -1);
  }
  else
  {
    printf("Error music %s\n", SDL_GetError());
  }

  while (run /*&& !pg->IsEndGame()*/)
  {
    auto new_ticks = SDL_GetTicks64();
    EventHandler();
    pg->Run();

    RenderScene();
    SDL_GL_SwapWindow(screen);

    auto end_ticks = SDL_GetTicks64();
    if (end_ticks - new_ticks < 50)
    {
      SDL_Delay(50 - (end_ticks - new_ticks));
    }
  }

  if (mus)
  {
    Mix_FreeMusic(mus);
  }

  cleanup();
  return 0;
}
