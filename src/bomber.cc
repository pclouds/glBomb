#include "bomber.h"

#include <SDL2/SDL.h>
#include <slang.h>

#include "actor.h"
#include "bomb.h"
#include "inputman.h"
#include "playground.h"
#include "renderman.h"
#include "screen.h"

// BOMBER:
// Đây là nhân vật chính của trò chơi :)

IMPLEMENT(Bomber, Object);

Base* Bomber::Create(Base* p)
{
  Object::Create(p);
  dx = 20;
  dy = 20;
  ChangeState(MSTATE_STAND);
  state |= STATE_DIRECT;  // | STATE_PUSH;
  move_thresold = 2;
  blood = 1;
  bomb_power = 1;
  return this;
}

Bomber::~Bomber()
{
}

int Bomber::DoFillInfo(ObjectResource* info)
{
  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND, 1, 39, 1);

  info->mstate[MSTATE_MOVE] = new FState(MSTATE_MOVE, 40, 45, 1);

  info->mstate[MSTATE_BOMB] = new FState(MSTATE_BOMB, 124, 135, 1);
  info->mstate[MSTATE_BOMB]->next = info->mstate[MSTATE_STAND];

  info->mstate[MSTATE_PAIN] = new FState(MSTATE_PAIN, 55, 58, 1);
  info->mstate[MSTATE_PAIN]->next = info->mstate[MSTATE_STAND];
  info->mstate[MSTATE_PAIN2] = new FState(MSTATE_PAIN, 59, 62, 1);
  info->mstate[MSTATE_PAIN2]->next = info->mstate[MSTATE_STAND];
  info->mstate[MSTATE_PAIN3] = new FState(MSTATE_PAIN, 63, 66, 1);
  info->mstate[MSTATE_PAIN3]->next = info->mstate[MSTATE_STAND];

  info->mstate[MSTATE_DEAD] =
      new FState(MSTATE_DEAD, 179, 184, 1, FSTATE_NOBREAK);
  info->mstate[MSTATE_DEAD2] =
      new FState(MSTATE_DEAD, 185, 190, 1, FSTATE_NOBREAK);
  info->mstate[MSTATE_DEAD3] =
      new FState(MSTATE_DEAD, 191, 198, 1, FSTATE_NOBREAK);

  info->mstate[MSTATE_PUSH] = new FState(MSTATE_PUSH, 47, 54, 1);
  info->mstate[MSTATE_PUSH]->next = info->mstate[MSTATE_STAND];

  info->mstate[MSTATE_GIFT] = new FState(MSTATE_GIFT, 73, 84, 1);
  //  info->mstate[MSTATE_GIFT]->next = info->mstate[MSTATE_STAND];

  if (!info->md2_filename)
  {
    info->md2_filename = newstrdup("pknight");
  }

  //  info->md2_model->LoadModel("data/models/crafty/tris.md2","data/models/crafty/tris.pcx");
  //  info->md2_model->LoadWeapon("data/models/crafty/weapon.md2","data/models/crafty/tris.pcx");
  //  info->md2_model->LoadModel("data/models/sodf8/tris.md2","data/models/sodf8/tris.pcx");
  //  info->md2_model->LoadWeapon("data/models/sodf8/weapon.md2","data/models/sodf8/tris.pcx");
  return 1;
}

int Bomber::DoAction(Object* obj, int verb, void* data)
{
  FMachine* fmachine;
  switch (verb)
  {
    case VERB_PUSH:
      ChangeState(MSTATE_PUSH);
      break;

    case VERB_BOMB_MAKE:
      ChangeState(MSTATE_BOMB);
      break;
    case VERB_END_STATE:
      fmachine = (FMachine*)data;
      switch (fmachine->current->id)
      {
        case MSTATE_BOMB:
          MakeBomb();
          break;
        case MSTATE_PUSH:
        {
          Position p;
          Playground* pg = GetPlayground();
          GetPosition(p);
          p += next_position[direction];
          if (pg->Exist(p))
          {
            pg->c(p)->Action(this, VERB_PUSH, (void*)direction);
          }
        }
        break;
        default:
          Object::DoAction(obj, verb, data);
      }
      break;

    case VERB_GIFT_BOMB:
      bomb_power++;
      break;

    default:
      return Object::DoAction(obj, verb, data);
  }
  return 1;
}

void Bomber::MakeBomb()
{
  Playground* pg;
  Position p;

  pg = GetPlayground();
  if (!pg)
  {
    return;
  }
  if (!GetPosition(p))
  {
    return;
  }

  p += next_position[direction];

  if (pg->Exist(p))
  {
    Cell& c = *pg->c(p);
    if (!c.Touch(this))
    {
      for (int i = 0; i < MAX_OBJ; i++)
      {
        if (c.Exist(i) && c[i]->MyClass() == Bomb::Class())
        {
          return;
        }
      }
      (new Bomb(bomb_power))->Create(&c);
    }
  }
}

int Bomber::Serialize(lisp_object_t* cons, int store)
{
  Object::Serialize(cons, store);
  return 1;
}

void Bomber::DoRun()
{
}

IMPLEMENT(Actor, Bomber);

Base* Actor::Create(Base* p)
{
  Bomber::Create(p);
  IM_Connect(this);
  return this;
}

Actor::~Actor()
{
  Playground* pg = GetPlayground();
  if (pg)
  {
    pg->Action(this, VERB_END_GAME, (void*)nullptr);
  }
  IM_Disconnect(this);
}

void Actor::ConnectRenderer()
{
  RM_Connect(RENDER_ENTITY, this);
  RM_Connect(RENDER_SCREEN, this);
}

void Actor::DisconnectRenderer()
{
  RM_Disconnect(RENDER_ENTITY, this);
  RM_Disconnect(RENDER_SCREEN, this);
}

int Actor::DoFillInfo(ObjectResource* info)
{
  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND, 1, 39, 1);

  info->mstate[MSTATE_MOVE] = new FState(MSTATE_MOVE, 40, 45, 1);

  info->mstate[MSTATE_BOMB] = new FState(MSTATE_BOMB, 124, 135, 1);
  info->mstate[MSTATE_BOMB]->next = info->mstate[MSTATE_STAND];

  info->mstate[MSTATE_PAIN] = new FState(MSTATE_PAIN, 55, 58, 1);
  info->mstate[MSTATE_PAIN]->next = info->mstate[MSTATE_STAND];
  info->mstate[MSTATE_PAIN2] = new FState(MSTATE_PAIN, 59, 62, 1);
  info->mstate[MSTATE_PAIN2]->next = info->mstate[MSTATE_STAND];
  info->mstate[MSTATE_PAIN3] = new FState(MSTATE_PAIN, 63, 66, 1);
  info->mstate[MSTATE_PAIN3]->next = info->mstate[MSTATE_STAND];

  info->mstate[MSTATE_DEAD] =
      new FState(MSTATE_DEAD, 179, 184, 1, FSTATE_NOBREAK);
  info->mstate[MSTATE_DEAD2] =
      new FState(MSTATE_DEAD, 185, 190, 1, FSTATE_NOBREAK);
  info->mstate[MSTATE_DEAD3] =
      new FState(MSTATE_DEAD, 191, 198, 1, FSTATE_NOBREAK);

  info->mstate[MSTATE_PUSH] = new FState(MSTATE_PUSH, 47, 54, 1);
  info->mstate[MSTATE_PUSH]->next = info->mstate[MSTATE_STAND];

  info->mstate[MSTATE_GIFT] = new FState(MSTATE_GIFT, 73, 84, 1);
  //  info->mstate[MSTATE_GIFT]->next = info->mstate[MSTATE_STAND];

  info->md2_model = new Cone3DMD2;
  //  info->md2_model->LoadModel("data/models/yoshi/tris.md2","data/models/yoshi/tris.pcx");
  info->md2_model->LoadModel("data/models/pknight/tris.md2",
                             "data/models/pknight/tris.pcx");
  info->md2_model->LoadWeapon("data/models/pknight/weapon.md2",
                              "data/models/pknight/weapon.pcx");
  //  info->md2_model->LoadModel("data/models/crafty/tris.md2","data/models/crafty/tris.pcx");
  //  info->md2_model->LoadWeapon("data/models/crafty/weapon.md2","data/models/crafty/tris.pcx");
  //  info->md2_model->LoadModel("data/models/sodf8/tris.md2","data/models/sodf8/tris.pcx");
  //  info->md2_model->LoadWeapon("data/models/sodf8/weapon.md2","data/models/sodf8/tris.pcx");

  return 1;
}

int Actor::DoAction(Object* obj, int verb, void* data)
{
  switch (verb)
  {
    case VERB_INPUT:
      return InputAction((SDL_Event*)data);
      break;
    default:
      return Bomber::DoAction(obj, verb, data);
  }
  return 1;
}

int Actor::InputAction(SDL_Event* ev)
{
  switch (ev->type)
  {
    case SDL_KEYDOWN:
      if (ev->key.keysym.mod == 0)
      {
        switch (ev->key.keysym.sym)
        {
          case SDLK_UP:
            state |= STATE_ACTOR_MOVING;
            Action(this, VERB_MOVE, (void*)UP);
            break;
          case SDLK_DOWN:
            Action(this, VERB_MOVE, (void*)DOWN);
            break;
          case SDLK_LEFT:
            Action(this, VERB_MOVE, (void*)LEFT);
            break;
          case SDLK_RIGHT:
            Action(this, VERB_MOVE, (void*)RIGHT);
            break;
          case SDLK_SPACE:
            Action(this, VERB_BOMB_MAKE, nullptr);
            break;
          case SDLK_RETURN:
            Action(this, VERB_PUSH, (void*)UP);
            break;
          default:
            return 1;
        }
      }
      return 0;

    case SDL_KEYUP:
      switch (ev->key.keysym.sym)
      {
        case SDLK_UP:
          state &= ~STATE_ACTOR_MOVING;
        case SDLK_DOWN:
        case SDLK_LEFT:
        case SDLK_RIGHT:
          Action(this, VERB_STAY, nullptr);
          break;
        default:
          return 1;
      }
      return 0;

    default:
      break;
  }
  return 1;
}

void Actor::DoRun()
{
  if (motion.current)
  {
    if (motion.current->id != MSTATE_MOVE)
    {
      if (state & STATE_ACTOR_MOVING)
      {
        Action(this, VERB_MOVE, (void*)UP);
      }
    }
    else
    {
      Position p;
      if (GetPosition(p))
      {
        RM_LookAt(p.x, p.y);
      }
    }
  }

  Bomber::DoRun();
}

void Actor::DoRender(int order)
{
  switch (order)
  {
    case RENDER_SCREEN:
      if (blood > 0)
      {
        glPushMatrix();
        font.SetSize(2, 2);
        glColor3d(0, 0, 1);
        font.DrawText(0,
                      "Blood %d Bomb %d %s",
                      blood,
                      bomb_power,
                      state & STATE_PUSH ? "Pushable" : "");
        glPopMatrix();
      }
      break;

    default:
      Bomber::DoRender(order);
      break;
  }
}

// Local Variables:
// End:
