#pragma once
#include "object.h"

class Brick : public Object
{
public:
  Base* Create(Base* p) override;
  ~Brick() override;

protected:
  int DoFillInfo(ObjectResource*) override;
  void DoRender(int order) override;
  int DoAction(Object* obj, int verb, void* data) override;

private:
  DECLARE(Brick, Object);
};

// Local Variables:
// mode: c++
// End:
