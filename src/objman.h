#pragma once
#include "object.h"
int ObjectManager_Init();
void ObjectManager_Cleanup();

int OM_Add(Object*);
int OM_Remove(Object*);
