#include "stone.h"

#include <GL/glu.h>

// STONE:
// Đây là phần tử "trơ", không có phản ứng gì trước các tác động của bên ngoài

IMPLEMENT(Stone, Object);

// char* Stone::Class() { return "StoneClass"; }
// static Object* DynamicCreate() { return new Stone; }

Base* Stone::Create(Base* p)
{
  Object::Create(p);
  blood = -1;
  ChangeState(MSTATE_STAND);
  return this;
}

Stone::~Stone()
{
  //  glDeleteLists(dplist,1);
}

int Stone::DoFillInfo(ObjectResource* info)
{
  /*
  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND,1,39,1);

  info->md2_model = new Cone3DMD2;
  info->md2_model->LoadModel("data/astro/weapon.md2", "data/astro/weapon.pcx");
  */
  //  info->Create = DynamicCreate;
  info->dplist = glGenLists(1);

  GLUquadricObj* obj;
  obj = gluNewQuadric();
  glNewList(info->dplist, GL_COMPILE);
  glColor3f(1, 0, 0);
  glTranslatef(0, -0.5, 0);
  glRotatef(-90, 1, 0, 0);
  gluCylinder(obj, .5, 0, 1, 3, 5);
  glEndList();
  gluDeleteQuadric(obj);

  return 1;
  /*
  int dplist;

  dplist = glGenLists(1);

  glNewList(dplist,GL_COMPILE);
  glBegin(GL_QUADS);
  glColor3f(1.0,0.0,1.0);
  glVertex3f(0,0,0);
  glVertex3f(1,0,0);
  glVertex3f(1,.5,0);
  glVertex3f(0,.5,0);

  glVertex3f(0,0,0);
  glVertex3f(0,.5,0);
  glVertex3f(0,.5,1);
  glVertex3f(0,0,1);

  glVertex3f(0,0,1);
  glVertex3f(0,.5,1);
  glVertex3f(1,.5,1);
  glVertex3f(1,0,1);

  glVertex3f(1,0,0);
  glVertex3f(1,0,1);
  glVertex3f(1,.5,1);
  glVertex3f(1,.5,0);

  glColor3f(0.5,0.5,0.5);
  glVertex3f(0,0.5,0);
  glVertex3f(1,0.5,0);
  glVertex3f(1,0.5,1);
  glVertex3f(0,0.5,1);
  glEnd();
  glEndList();

  return (void*)dplist;
  */
}

void Stone::DoRender(int order)
{
  glCallList(info->dplist);
  // Object::DoRender(int order);
}

int Stone::DoAction(Object* obj, int verb, void* data)
{
  switch (verb)
  {
    case VERB_TOUCH:
      return 1;

    default:
      return Object::DoAction(obj, verb, data);
  }
  return 0;
}

// Local Variables:
// End:
