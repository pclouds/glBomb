#pragma once
#include "object.h"

class Gate : public Object
{
public:
  Base* Create(Base* p) override;

protected:
  int DoFillInfo(ObjectResource*) override;
  void DoRender(int order) override;
  int DoAction(Object* obj, int verb, void* data) override;
  void DoRun() override;
  int new_creatures;

private:
  DECLARE(Gate, Object);
};

// Local Variables:
// mode: c++
// End:
