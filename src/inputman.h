#pragma once
#include <SDL2/SDL.h>

#include "object.h"

int IM_PushLayout();
int IM_PopLayout();
void IM_ResetLayout();
int IM_Connect(Object*);
int IM_Disconnect(Object*);

int InputManager_Init();
void InputManager_Cleanup();
void InputManager_Process(SDL_Event* ev);
void InputManager_Reset();
