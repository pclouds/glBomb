#ifndef __BASE_H__
#define __BASE_H__

#include "lispreader.h"
#include "resman.h"
#include "rtti.h"

class Base
{
public:
  virtual Base* Create(Base* parent)
  {
    return this;
  }
  virtual ~Base()
  {
  }
  static RTTI* Class();
  virtual RTTI* MyClass()
  {
    return Class();
  }

  lisp_object_t* StoreObject();
  static Base* LoadObject(lisp_object_t*, Base*);

protected:
  virtual int Serialize(lisp_object_t*, int store = 0);
};

// Misc structure
struct Position
{
  int x, y;

  Position()
  {
    x = 0;
    y = 0;
  }

  Position(int _x, int _y)
  {
    x = _x;
    y = _y;
  }

  Position(const Position& p)
  {
    x = p.x;
    y = p.y;
  }

  Position& operator+=(const Position& p)
  {
    x += p.x;
    y += p.y;
    return *this;
  }
};

class Object;
struct VerbInfo
{
  Object* sender;
  int verb;
  void* data;
};

#endif

// Local Variables:
// mode: c++
// End:
