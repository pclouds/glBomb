#pragma once
class Base;
Base* Res_Get(const char* locator);
int Res_Register(const char* locator, const Base* obj);

int ResManager_Init();
void ResManager_Cleanup();

// Local Variables:
// mode: c++
// End:
