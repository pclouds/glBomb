#include "brick.h"

#include <GL/glu.h>

// BRICK:
// Đây là phần tử thường gặp trong trò chơi.
// Ngăn cản (TOUCH) không cho đi xuyên qua (trừ khi có STATE_PASSBY).
// Chịu tác động (VERB_BOMB_BOOM) và biến mất.
// Chịu tác động của (VERB_PUSH). Khi đó sẽ tự động di chuyển và phát sinh
// OBSTACLE_PUSH khi đụng các vật thể khác.

IMPLEMENT(Brick, Object);

Base* Brick::Create(Base* p)
{
  return Object::Create(p);
}

Brick::~Brick()
{
}

int Brick::DoFillInfo(ObjectResource* info)
{
  info->dplist = glGenLists(1);

  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND, 0, 1, 1);
  info->mstate[MSTATE_MOVE] = new FState(MSTATE_MOVE, 0, 1, 1);

  GLUquadricObj* obj;
  obj = gluNewQuadric();
  glNewList(info->dplist, GL_COMPILE);
  glColor3f(0, 0, 0.7);
  glTranslatef(0, -0.5, 0);
  glRotatef(-90, 1, 0, 0);
  gluCylinder(obj, .5, 0, 1, 5, 10);
  glEndList();
  gluDeleteQuadric(obj);

  return 1;

  /*
  int id;
  id = glGenLists(1);

  glNewList(dplist,GL_COMPILE);
  glBegin(GL_QUADS);
  glColor3f(1.0,1.0,0.0);
  glVertex3f(0,0,0);
  glVertex3f(1,0,0);
  glVertex3f(1,.5,0);
  glVertex3f(0,.5,0);

  glVertex3f(0,0,0);
  glVertex3f(0,.5,0);
  glVertex3f(0,.5,1);
  glVertex3f(0,0,1);

  glVertex3f(0,0,1);
  glVertex3f(0,.5,1);
  glVertex3f(1,.5,1);
  glVertex3f(1,0,1);

  glVertex3f(1,0,0);
  glVertex3f(1,0,1);
  glVertex3f(1,.5,1);
  glVertex3f(1,.5,0);

  glColor3f(0.7,0.7,0.7);
  glVertex3f(0,0.5,0);
  glVertex3f(1,0.5,0);
  glVertex3f(1,0.5,1);
  glVertex3f(0,0.5,1);
  glEnd();
  glEndList();

  return (void*)id;
  */
}

void Brick::DoRender(int order)
{
  glCallList(info->dplist);
  // TODO
  // glCallList(dplist);
}

int Brick::DoAction(Object* obj, int verb, void* data)
{
  switch (verb)
  {
    case VERB_TOUCH:
      // nếu data != NULL nghĩa là có thể đi xuyên qua
      // Trả về non-zero nếu phải đứng lại (sờ thấy được)
      return 1;

    case VERB_BOMB_BOOM:
      SetLiveState(STATE_PREDEAD);
      break;

    default:
      return Object::DoAction(obj, verb, data);
  }
  return 1;
}

// Local Variables:
// End:
