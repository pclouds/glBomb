/***************************************************************************
                          cone3dpcx.cpp  -  pcxloader
                             -------------------
    begin                : Thu Dec 28 2000
    copyright            : (C) 2000 by Marius Andra
    email                : marius@hot.ee
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "cone3d.h"

typedef struct _pcxHeader
{
  short id[2];
  short offset[2];
  short size[2];
} pcxHeader;

Cone3DPCX::Cone3DPCX()
{
  imageData = nullptr;
}

Cone3DPCX::~Cone3DPCX()
{
  if (imageData)
  {
    free(imageData);
  }
}

int Cone3DPCX::LoadPCX(const char* filename, int alpha, int color)
{
  FILE* hTexFile = fopen(filename, "rb");

  alpha = 1;

  /* check the file open command */
  if (hTexFile != nullptr)
  {
    int imgWidth, imgHeight, texFileLen, imgBufferPtr, i;
    pcxHeader* pcxPtr;
    unsigned char *imgBuffer, *texBuffer, *pcxBufferPtr, *paletteBuffer;

    /* find length of file */
    fseek(hTexFile, 0, SEEK_END);
    texFileLen = ftell(hTexFile);
    fseek(hTexFile, 0, SEEK_SET);

    /* read in file */
    texBuffer = (unsigned char*)malloc(texFileLen + 1);
    fread(texBuffer, sizeof(char), texFileLen, hTexFile);

    /* get the image dimensions */
    pcxPtr = (pcxHeader*)texBuffer;
    imgWidth = pcxPtr->size[0] - pcxPtr->offset[0] + 1;
    imgHeight = pcxPtr->size[1] - pcxPtr->offset[1] + 1;

    /* image starts at 128 from the beginning of the buffer */
    imgBuffer = (unsigned char*)malloc(imgWidth * imgHeight);
    imgBufferPtr = 0;
    pcxBufferPtr = &texBuffer[128];

    /* decode the pcx image */
    while (imgBufferPtr < (imgWidth * imgHeight))
    {
      if (*pcxBufferPtr > 0xbf)
      {
        int repeat = *pcxBufferPtr++ & 0x3f;
        for (i = 0; i < repeat; i++)
        {
          imgBuffer[imgBufferPtr++] = *pcxBufferPtr;
        }
      }
      else
      {
        imgBuffer[imgBufferPtr++] = *pcxBufferPtr;
      }
      pcxBufferPtr++;
    }
    /* read in the image palette */
    paletteBuffer = (unsigned char*)malloc(768);
    for (i = 0; i < 768; i++)
    {
      paletteBuffer[i] = texBuffer[texFileLen - 768 + i];
    }

    width = imgWidth;
    height = imgHeight;

    /* now create the OpenGL texture */
    {
      int i, j, sz = alpha ? 4 : 3;
      imageData = (unsigned char*)malloc(width * height * sz);
      for (j = 0; j < imgHeight; j++)
      {
        for (i = 0; i < imgWidth; i++)
        {
          imageData[sz * (j * width + i) + 0] =
              paletteBuffer[3 * imgBuffer[j * imgWidth + i] + 0];
          imageData[sz * (j * width + i) + 1] =
              paletteBuffer[3 * imgBuffer[j * imgWidth + i] + 1];
          imageData[sz * (j * width + i) + 2] =
              paletteBuffer[3 * imgBuffer[j * imgWidth + i] + 2];
          if (alpha)
          {
            imageData[sz * (j * width + i) + 3] =
                paletteBuffer[3 * imgBuffer[j * imgWidth + i] + 2];
          }
          // imgBuffer[j*imgWidth+i] == color ? 0 : 255;
        }
      }
    }
    /* cleanup */
    free(paletteBuffer);
    free(imgBuffer);
    glGenTextures(1, &texID);
    glBindTexture(GL_TEXTURE_2D, texID);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 alpha ? GL_SRGB_ALPHA : GL_SRGB,
                 width,
                 height,
                 0,
                 alpha ? GL_RGBA : GL_RGB,
                 GL_UNSIGNED_BYTE,
                 imageData);
  }
  else
  {
    /* skip the texture setup functions */
    return 0;
  }
  bpp = 24;

  return 1;
}
