#pragma once
#include "object.h"

class Explosion : public Object
{
public:
  Explosion(int _wait = 0)
  {
    wait = _wait;
  }
  Base* Create(Base* p) override;
  ~Explosion() override;

protected:
  int DoFillInfo(ObjectResource*) override;
  void DoRender(int order) override;
  int DoAction(Object* obj, int verb, void* data) override;
  //  void DoRun();

  void ConnectRenderer() override;
  void DisconnectRenderer() override;

  int thresold;
  int wait;
  //  int pieces[3][MAX_PIECES];

private:
  DECLARE(Explosion, Object);
};

// Local Variables:
// mode: c++
// End:
