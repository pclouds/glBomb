#pragma once
#include "object.h"

class Gift : public Object
{
public:
  Base* Create(Base* p) override;
  ~Gift() override;

protected:
  int DoFillInfo(ObjectResource*) override;
  int DoAction(Object* obj, int verb, void* data) override;
  void DoRender(int order) override;

private:
  DECLARE(Gift, Object);
};

// Local Variables:
// mode: c++
// End:
