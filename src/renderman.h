#pragma once
#include "object.h"

#define RENDER_LANDSCAPE 0
#define RENDER_ENTITY 1
#define RENDER_SCREEN 2

int RenderManager_Init();
void RenderManager_Cleanup();
void RenderManager_Reset();
void RenderManager_Render(int order);

int RM_Connect(int order, Object*);
void RM_Disconnect(int order, Object*);
void RM_LookAt(int x, int y);
int RM_LookAtX();
int RM_LookAtY();
