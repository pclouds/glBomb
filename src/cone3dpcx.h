/***************************************************************************
                          texture.h  -  description
                             -------------------
    begin                : Thu Dec 28 2000
    copyright            : (C) 2000 by Marius Andra
    email                : marius@hot.ee
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef __CONE3D_PCX_H__
#define __CONE3D_PCX_H__

#include <GL/glu.h>

class Cone3DPCX
{
public:
  Cone3DPCX();
  ~Cone3DPCX();

  int LoadPCX(const char* filename, int alpha = 0,
              int color = 0);  // PCX loader

  unsigned int bpp;
  unsigned int width;
  unsigned int height;
  unsigned int texID;

  GLubyte* imageData;
};

#endif
