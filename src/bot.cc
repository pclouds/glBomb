#include "bot.h"

#include <stdlib.h>

IMPLEMENT(Bot, Bomber);

Bot::~Bot()
{
}

Base* Bot::Create(Base* p)
{
  Bomber::Create(p);
  blood = 1;
  return this;
}

int Bot::DoFillInfo(ObjectResource* info)
{
  RTTI* rtti = RTTI::Get("Bot");
  Bomber::DoFillInfo(info);
  /*
  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND,1,39,1);
  info->mstate[MSTATE_MOVE] = new FState(MSTATE_MOVE,40,45,1);
  info->mstate[MSTATE_BOMB] = new FState(MSTATE_BOMB,124,135,1);
  info->mstate[MSTATE_BOMB]->next = info->mstate[MSTATE_STAND];
  info->mstate[MSTATE_PAIN] = new FState(MSTATE_PAIN,55,58,1);
  info->mstate[MSTATE_PAIN]->next = info->mstate[MSTATE_STAND];
  info->mstate[MSTATE_DEAD] = new FState(MSTATE_DEAD,179,184,1,FSTATE_NOBREAK);
  */
  if (!info->md2_filename)
  {
    info->md2_filename = newstrdup("yoshi");
  }

  // info->md2_model->LoadModel("data/models/pknight/tris.md2","data/models/yoshi/tris.pcx");
  return 1;
}

int Bot::DoAction(Object* obj, int verb, int* data)
{
  //  switch (obj) {

  return Bomber::DoAction(obj, verb, data);
  //  }
}

// Move, Stay, MakeBomb(?),
void Bot::DoRun()
{
  /*
    if ((rand() % 13) == 0)
    Action(this,VERB_FIGHT,NULL);
  */
  if ((rand() % 15) == 0)
  {
    Action(this, VERB_STAY, nullptr);
  }

  if ((rand() % 17) == 0)
  {
    if ((rand() % 2) == 0)
    {
      Action(this, VERB_MOVE, (void*)UP);
    }
    else
    {
      Action(this, VERB_MOVE, (void*)(rand() % 4));
    }
  }
  Bomber::DoRun();
}
