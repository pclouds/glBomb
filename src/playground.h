#pragma once

#include <array>
#include <memory>
#include <vector>

#include "cell.h"

class Playground : public Object
{
public:
  Playground(int screenWidth, int screenHeight);

  int InitGame();
  void CleanupGame();

  int NewGame(int w, int h);
  int CreateRandom();
  int IsEndGame();
  int IsWin()
  {
    return state & STATE_WIN_GAME;
  }

  int SetActor(Object* o);
  Object* GetActor(int id)
  {
    return (id >= 0 && id <= m_actors.size()) ? m_actors[id] : nullptr;
  }

  std::shared_ptr<Cell> c(int x, int y)
  {
    return m_table[y * m_width + x];
  }
  std::shared_ptr<Cell> c(const Position& p)
  {
    return c(p.x, p.y);
  }
  int Exist(int x, int y);
  int Exist(const Position& p)
  {
    return Exist(p.x, p.y);
  }

  int width() const
  {
    return m_width;
  }

  int height() const
  {
    return m_height;
  }

  int SetTexture()
  {
    if (!m_texture.imageData)
    {
      return 0;
    }
    glBindTexture(GL_TEXTURE_2D, m_texture.texID);
    return 1;
  }

private:
  void DoRender(int order) override;
  int DoAction(Object* obj, int verb, void* data) override;
  void DoRun() override;

  std::vector<std::shared_ptr<Cell>> m_table;
  int m_width{};
  int m_height{};
  int effect{};
  int m_screenWidth;
  int m_screenHeight;
  std::array<Object*, MAX_ACTOR> m_actors{};
  Cone3DPCX m_texture;
};

// Local Variables:
// mode: c++
// End:
