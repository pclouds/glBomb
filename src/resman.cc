#include "resman.h"

#include "base.h"

struct Item
{
  Base* obj{};
  char* locator{};
};
static Item* resource;
static int nr_resource;

int ResManager_Init()
{
  nr_resource = 100;
  resource = new Item[nr_resource];
  return 1;
}

void ResManager_Cleanup()
{
  delete[] resource;
}

Base* Res_Get(const char* locator)
{
  for (int i = 0; i < nr_resource; i++)
  {
    if (resource[i].locator == locator)
    {
      return resource[i].obj;
    }
  }
  return nullptr;
}

int Res_Register(const char* locator, const Base* obj)
{
  for (int i = 0; i < nr_resource; i++)
  {
    if (resource[i].locator == nullptr)
    {
      resource[i].obj = (Base*)obj;
      resource[i].locator = (char*)locator;
      return 1;
    }
  }
  return 0;
}
