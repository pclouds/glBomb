#pragma once
#include <SDL2/SDL_mixer.h>

#include "base.h"

class FState : public Base
{
  DECLARE(FState, Base);

public:
  int id;  // for convenient
  int start, end, step, delay;
  FState* next;
  VerbInfo verb;
  int state;  // BREAKABLE
  Mix_Chunk* sfx;
  FState();
  FState(int _id, int _start, int _end, int _delay, int _state = 0)
  {
    sfx = nullptr;
    next = this;
    //    FState(); // Neu de thi hang!! ??
    id = _id;
    start = _start;
    end = _end;
    delay = _delay;
    state = _state;
  }

  ~FState() override
  {
    if (sfx)
    {
      Mix_FreeChunk(sfx);
    }
  }

protected:
  int Serialize(lisp_object_t*, int) override;
};

class FMachine : public Base
{
public:
  FState* current;
  Object* host;
  int step, delay;

  Base* Create(Base*) override;
  ~FMachine() override
  {
  }
  int Change(FState* state);
  void Step();
  virtual void EndOfState();
  virtual void BeginOfState();
  virtual void RewindState();

private:
  DECLARE(FMachine, Base);
};

// Local Variables:
// mode: c++
// End:
