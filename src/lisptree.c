#include "lisptree.h"

#include <string.h>

lisp_object_t* lisp_make_object(const char* type)
{
  return lisp_make_cons(lisp_make_symbol(type), lisp_nil());
}

void lisp_add_child(lisp_object_t* cons, lisp_object_t* child)
{
  lisp_subst_cdr(cons, lisp_make_cons(child, lisp_cdr(cons)));
}

lisp_object_t* lisp_make_prop(const char* name, lisp_object_t* value)
{
  return lisp_make_cons(lisp_make_symbol(name), lisp_make_cons(value, NULL));
}

lisp_object_t* lisp_get_object(lisp_object_t* cons, char* type)
{
  if (!lisp_cons_p(cons))
    return NULL;
  if (!lisp_symbol_p(lisp_car(cons)))
    return NULL;
  if (type)
    strcpy(type, lisp_symbol(lisp_car(cons)));
  return lisp_cdr(cons);
}

lisp_object_t* lisp_get_prop(lisp_object_t* cons, const char* name)
{
  lisp_object_t* prop;

  while (cons)
  {
    prop = lisp_car(cons);
    if (lisp_symbol_p(lisp_car(prop)) &&
        !strcmp(lisp_symbol(lisp_car(prop)), name))
      return lisp_car(lisp_cdr(prop));
    cons = lisp_cdr(cons);
  }
  return NULL;
}

lisp_object_t* lisp_get_cons_prop(lisp_object_t* cons, const char* name)
{
  lisp_object_t* prop;

  while (cons)
  {
    prop = lisp_car(cons);
    if (lisp_symbol_p(lisp_car(prop)) &&
        !strcmp(lisp_symbol(lisp_car(prop)), name))
      return lisp_cdr(prop);
    cons = lisp_cdr(cons);
  }
  return NULL;
}
