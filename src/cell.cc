#include "cell.h"

#include <stdlib.h>
#include <string.h>

#include "playground.h"

IMPLEMENT(Cell, Object);

#define MSTATE_DANGEROUS 1

Base* Cell::Create(Base* p)
{
  Object::Create(p);
  ChangeState(MSTATE_STAND);
  if (info->mstate[MSTATE_STAND] == nullptr)
  {
    exit(0);
  }

  for (int i = 0; i < MAX_OBJ; i++)
  {
    obj[i] = nullptr;
    destroyed_obj[i] = 0;
  }

  return this;
}

int Cell::DoFillInfo(ObjectResource* info)
{
  info->mstate[MSTATE_STAND] = new FState(MSTATE_STAND, 1, 1, 3);
  info->mstate[MSTATE_DANGEROUS] = new FState(MSTATE_DANGEROUS, 1, 3, 3);
  info->mstate[MSTATE_DANGEROUS]->next = info->mstate[MSTATE_STAND];
  return 1;
}

void Cell::DoRender(int order)
{
  Playground* pg = CAST(Playground, parent);
  int has_texture = pg->SetTexture();

  glPushAttrib(GL_ENABLE_BIT | GL_LIGHTING_BIT);
  if (has_texture)
  {
    glEnable(GL_TEXTURE_2D);
  }

  glBegin(GL_QUADS);
  if (motion.current && motion.current->id == MSTATE_DANGEROUS)
  {
    GLfloat emission[] = {0, 1, 0, 1};
    glColor3f(0.0, 1.0, 0.0);
    glMaterialfv(GL_FRONT, GL_EMISSION, emission);
  }
  else
  {
    glColor3f(1.0, 1.0, 1.0);
  }

  if (has_texture)
  {
    int w = pg->width();
    int h = pg->height();
    Position p;
    GetPosition(p);
    glTexCoord2f((GLfloat)p.x / w, (GLfloat)p.y / h);
    // glTexCoord2f(0,0);
    glVertex3f(-0.5, -0.5, -0.5);
    glTexCoord2f((GLfloat)p.x / w, (GLfloat)(p.y + 1) / h);
    // glTexCoord2f(0,1);
    glVertex3f(-0.5, -0.5, 0.5);
    glTexCoord2f((GLfloat)(p.x + 1) / w, (GLfloat)(p.y + 1) / h);
    // glTexCoord2f(1,1);
    glVertex3f(0.5, -0.5, 0.5);
    glTexCoord2f((GLfloat)(p.x + 1) / w, (GLfloat)p.y / h);
    // glTexCoord2f(1,0);
    glVertex3f(0.5, -0.5, -0.5);
  }
  else
  {
    glVertex3f(-0.5, -0.5, -0.5);
    glVertex3f(-0.5, -0.5, 0.5);
    glVertex3f(0.5, -0.5, 0.5);
    glVertex3f(0.5, -0.5, -0.5);
  }
  glEnd();
  glPopAttrib();

  /*
    for (int i = 0;i < MAX_OBJ;i ++) {
    if (obj[i])
    obj[i]->Render(order);
    }
  */
}

int Cell::DoAction(Object* o, int verb, void* data)
{
  int i, ret;

  switch (verb)
  {
    case VERB_TOUCH:
      for (i = 0; i < MAX_OBJ; i++)
      {
        if (obj[i])
        {
          if ((ret = obj[i]->Action(o, verb, data)))
          {
            return 1;
          }
        }
      }
      return 0;

    case VERB_IN:
      for (i = 0; i < MAX_OBJ; i++)
      {
        if (!obj[i])
        {
          obj[i] = o;
          Object::DoAction(o, verb, data);
          break;
        }
      }
      break;

    case VERB_OUT:
      for (i = 0; i < MAX_OBJ; i++)
      {
        if (obj[i] == o)
        {
          obj[i] = nullptr;
          Object::DoAction(o, verb, data);
          break;
        }
      }
      break;

    case VERB_POS:
      for (i = 0; i < MAX_OBJ; i++)
      {
        if (obj[i] == o)
        {
          return parent->Action(this, verb, data);
        }
      }
      return 0;

    case VERB_PLAYGROUND:
      *((Object**)data) = parent;
      ret = 1;
      break;

    case VERB_DESTROY:
      for (i = 0; i < MAX_OBJ; i++)
      {
        if (obj[i] == o)
        {
          destroyed_obj[i] = 1;
          parent->Action(o, VERB_DESTROY, nullptr);
          break;
        }
      }
      break;

    case VERB_PUSH:
    case VERB_GO:
    case VERB_BOMB_BOOM:
      for (i = 0; i < MAX_OBJ; i++)
      {
        if (obj[i] != nullptr)
        {
          obj[i]->Action(o, verb, data);
        }
      }
      break;

    case VERB_DANGEROUS:
      if (data != nullptr)
      {
        ChangeState(MSTATE_DANGEROUS);
      }
      else
      {
        ChangeState(MSTATE_STAND);
      }
      break;

    default:
      return Object::DoAction(o, verb, data);
  }
  return 1;
}

void Cell::DoRun()
{
  int i;
  for (i = 0; i < MAX_OBJ; i++)
  {
    if (destroyed_obj[i])
    {
      delete obj[i];
      obj[i] = nullptr;
      destroyed_obj[i] = 0;
    }
  }

  for (i = 0; i < MAX_OBJ; i++)
  {
    if (obj[i])
    {
      obj[i]->Run();
    }
  }

  //  state &= ~STATE_DANGEROUS;
}

int Cell::Serialize(lisp_object_t* cons, int store)
{
  lisp_object_t* c = lisp_nil();
  if (store)
  {
    for (int i = 0; i < MAX_OBJ; i++)
    {
      if (Exist(i) && !destroyed_obj[i])
      {
        c = lisp_make_cons((*this)[i]->StoreObject(), c);
      }
    }
    StoreData(cons, "objs", LISP_TYPE_CONS, &c);
  }
  else
  {
    LoadData(cons, "objs", LISP_TYPE_CONS, &c);
    while (!lisp_nil_p(c))
    {
      Base* o = LoadObject(lisp_car(c), this);
      c = lisp_cdr(c);
    }
  }
  return Object::Serialize(cons, store);
}
