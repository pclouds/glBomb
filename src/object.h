#pragma once

#include <GL/gl.h>
#include <SDL2/SDL_thread.h>
#include <stddef.h>

#include "base.h"
#include "cone3dmd2.h"
#include "define.h"
#include "fmachine.h"
#include "lispreader.h"
#include "misc.h"

class Cell;
class Playground;
class Object;

class ObjectResource : public Base
{
public:
  FState* mstate[MAX_FSTATE];
  FState* astate[MAX_FSTATE];  // Not used
  char* md2_filename;
  char* sfx_filename[MAX_FSTATE];
  Cone3DMD2* md2_model;
  GLfloat md2_scale;
  int dplist;
  //  Object* (*Create)();
  //  GLfloat color[4];

  ObjectResource();
  ~ObjectResource() override;
  int Serialize(lisp_object_t* cons, int store) override;

private:
  DECLARE(ObjectResource, Base);
};

/* Drawable,moveable object */
class Object : public Base
{
public:
  Base* Create(Base* p) override;
  ~Object() override;

  virtual int CanRender();
  virtual int CanAction(Object* obj, int verb, void* data);
  virtual int CanRun();

  void Render(int order);
  int Action(const VerbInfo& vi)
  {
    return Action(vi.sender, vi.verb, vi.data);
  }
  int Action(Object* obj, int verb, void* data);
  void Run();

  int State()
  {
    return state;
  }
  int MotionState()
  {
    return motion.current ? motion.current->id : 0;
  }
  int Direction()
  {
    return direction;
  }
  int Blood()
  {
    return blood;
  }

  // convenient functions
  Playground* GetPlayground();
  int GetPosition(Position&);
  virtual int SetLiveState(int state);
  int Touch(Object* o)
  {
    return Action(o, VERB_TOUCH, (void*)(state & STATE_PASSBY));
  }

protected:
  Object* parent;  // = Cell*
  Position pos;

  int dx, dy;     // Độ tăng x,y
  int x, y;       // Tọa độ tính theo gốc của cell
  int direction;  // Hướng thực
  int state;
  int move_step, move_thresold;
  int move_distance;
  int blood;

  FMachine motion;
  FMachine animation;  // Not used
  int ChangeState(int state);
  virtual int FillInfo(ObjectResource* info);
  virtual int DoFillInfo(ObjectResource* info);

  int verb_mask[MAX_VERB];

  int Serialize(lisp_object_t*, int store = 0) override;
  friend lisp_object_t* StoreObject(Object*);
  friend Object* LoadObject(lisp_object_t*, Object*);

  ObjectResource* info;
  //  ObjectResource* CheckRegister(char *);
  //  ObjectResource* CheckRegister();
  //  virtual ObjectResource* NewInfo() {return new ObjectResource;}
  //  virtual void DoFillInfo(ObjectResource *info);

  void Move(Object* o, int direction);
  void SetPos(int x, int y);

  virtual void DoRender(int order);
  virtual int DoAction(Object* obj, int verb, void* data);
  virtual void DoRun();

  virtual void ConnectRenderer();
  virtual void DisconnectRenderer();

  friend class Cell;
  friend class Gift;

private:
  DECLARE(Object, Base);
};

// #define MAX_PIECES 10

extern Position next_position[4];
int common_init();
void common_cleanup();

int LoadData(lisp_object_t* children, const char* name, int type, void* value);
void StoreData(lisp_object_t*, const char* name, int type, void* value);

struct PROPS
{
  const char* name;
  int type;
  void* value;
};

#define BEGIN_PROPS(variable) PROPS variable[] = {
#define END_PROPS() \
  {                 \
    NULL, 0, NULL   \
  }                 \
  }

#define PROP_INT(name, var) {name, LISP_TYPE_INTEGER, &var},
#define PROP_REAL(name, var) {name, LISP_TYPE_REAL, &var},
#define PROP_CONS(name, var) {name, LISP_TYPE_CONS, &var},
#define PROP_STR(name, var) {name, LISP_TYPE_STRING, &var},

#define SERIALIZE(variable)                                                   \
  if (store)                                                                  \
  {                                                                           \
    for (int i = 0; variable[i].name; i++)                                    \
      StoreData(cons, variable[i].name, variable[i].type, variable[i].value); \
  }                                                                           \
  else                                                                        \
  {                                                                           \
    for (int i = 0; variable[i].name; i++)                                    \
      LoadData(cons, variable[i].name, variable[i].type, variable[i].value);  \
  }

// Local Variables:
// mode: c++
// End:
